# Demo
http://141.20.37.114
old: https://daidalos-backend.firebaseapp.com/

# Building locally

Clone the repo, and:

```sh
npm install
npm run build
```

You can run the development server with:

```sh
npm start
```

# DaidalosWeb

## Ansatz 
Die Oberfläche ist reine statische HTML-Seite, kann also "serverless" funktionieren. Das bedeutet, es reicht, die Dateien entweder auf eigener Festplatte zu haben oder mit z.B. Apache HTTP Server zu serven.

## Single Page Application 
Es ist auch ein SPA https://en.wikipedia.org/wiki/Single-page_application. Alle HTTP requests zum Server (auch Subpages) müssen zur index.html Datei im www-root Verzeichnis (my.domain.com/ bzw. my.domain.com/index.html) weitergeleitet werden. 

Beispiel mit Apache .htaccess:
```
RewriteEngine On
RewriteCond %{REQUEST_URI} !^/index.html$
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_URI} !\.(css|gif|ico|jpg|js|png|swf|txt|svg|woff|ttf|eot)$
RewriteRule . index.html [L]
```

## Routing
Routing erfolgt auf der client-Seite und einzelne Seiten werden für die entsprechende /routes/ angezeigt. Die Routes sind in der x-app.js Datei spezifiziert.

## Hintergrund und Aufbau der App
Wir haben lit-element https://lit-element.polymer-project.org/ Bibliothek von Google benutzt, um Custom HTML Components https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements zu bauen. Die App ist ein LitElement und alle verfügbare Seiten sind auch LitElements bzw. erben von dieser Klasse. 

### Webpack
Wir benutzen Webpack https://webpack.js.org/ um alle JS Abhängigkeiten (npm modules, css styles) zu bündeln.

### FetchElement
FetchElement ist unsere eigene ES6 Klasse, die von LitElement erbt, fetcht https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch asynchron notwendige Ressourcen und ladet dann weitere LitElements, wie Tabellen oder Formulare (oder Fehlermeldungen). Wir benutzen XHR nicht. Nach der Änderung der Daten schicken wir POST HTTP Requests auch mithilfe von fetch().

## Oberfläche und Aussehen der App
Die App implementiert Material Design style https://material.io/design/ von Google. Wir benutzen vanilla JS/css-Implementierung von Material Design Components https://github.com/material-components/material-components-web. Wir haben insbesondere TopAppBar https://github.com/material-components/material-components-web/tree/master/packages/mdc-top-app-bar und Drawer https://github.com/material-components/material-components-web/tree/master/packages/mdc-drawer components benutzt. 

Die Inhalte von Drawer werden dynamisch geladen.

### Tabellen und Formulare
Dafür haben wir Vaadin Components wie Vaadin Grid https://vaadin.com/components/vaadin-grid/ und Vaadin Form Layout https://vaadin.com/components/vaadin-form-layout benutzt. 
