const fs = require('fs');
// const path = require('path');
const webpack = require('webpack');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const cleanPlugin = require('clean-webpack-plugin');
const copyPlugin = require('copy-webpack-plugin');
const htmlPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const APP_PUBLIC_URL = 'https://daidalos-backend.firebaseapp.com/'
const HU_SERVER_URL = 'http://141.20.37.114/'

const readJson = filename => JSON.parse(fs.readFileSync(filename));

module.exports = function (_, env) {
  const isProd = env.mode === 'production';

  return {
    mode: isProd ? 'production' : 'development',
    entry: './src/index.js',
    devtool: isProd ? 'source-map' : 'inline-source-map',
    output: {
      filename: isProd ? 'js/[name].[chunkhash:5].js' : '[name].js',
      chunkFilename: 'js/[id].[chunkhash:5].js',
      publicPath: '/',
    },

    devServer: {
      open: true,
      historyApiFallback: true, //rewrites all 404 to index.html
    },

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            'css-loader',
            {
              loader: 'sass-loader',
              options: { includePaths: ['./node_modules'] }
            }
          ]
        },
      ]
    },

    plugins: [
      new ProgressBarPlugin({
        format: '\u001b[90m\u001b[44mBuild\u001b[49m\u001b[39m [:bar] \u001b[32m\u001b[1m:percent\u001b[22m\u001b[39m (:elapseds) \u001b[2m:msg\u001b[22m\r',
        renderThrottle: 100,
        summary: false,
        clear: true
      }),

      new webpack.DefinePlugin({
        PACKAGE_VERSION: JSON.stringify(readJson('./package.json').version),
        BUILD_TIMESTAMP: JSON.stringify(new Date()),
        APP_HOST: isProd ? JSON.stringify(APP_PUBLIC_URL) : JSON.stringify('http://localhost:8080/'),
        HU_SERVER_URL: JSON.stringify(HU_SERVER_URL)
      }),

      // isProd && new webpack.optimize.SplitChunksPlugin({}),

      isProd && new cleanPlugin(),

      new copyPlugin([{ from: 'src/static', to: 'assets' }]),
      //TODO: delete this and use real API
      new copyPlugin([{ from: 'fakeApi', to: 'api' }]),

      // isProd && new webpack.optimize.SplitChunksPlugin({}),

      new htmlPlugin({
        filename: 'index.html',
        template: 'src/index.html',
      }),

      // For production builds, output module size analysis to build/report.html
      isProd && new BundleAnalyzerPlugin({
        analyzerMode: 'static',
        defaultSizes: 'gzip',
        openAnalyzer: false
      }),

    ].filter(Boolean), // Filter out any falsey plugin array entries.

  }
}
