import { LitElement, html } from 'lit-element';
import { FETCH_ERROR } from './templates';

export class FetchElement extends LitElement {
  static get properties() {
    return {
      layout: { type: Object },
    };
  }

  async connectedCallback(urls) {
    super.connectedCallback();
    // console.log(urls)
    this.fetchedData = {};
    let failedUrls = [];

    await Promise.all(
      Object.keys(urls).map(key =>
        fetch(urls[key])
          .then(response => response.json())
          .then(json => this.fetchedData[key] = json)
          .catch(error => {
            console.error(error, urls[key]);
            failedUrls.push(urls[key]);
          })
      )
    )
      .then(() => this.layout = () => failedUrls.length ? this.renderOnFetchError(failedUrls) : this.renderAfterFetch(this.fetchedData))
  }

  renderBeforeFetch() {
    return html`Loading...`;
  }

  renderAfterFetch() {
    return html`Method renderAfterFetch(fetchData) not implemented in component!`;
  }

  renderOnFetchError(failedUrls) {
    return FETCH_ERROR(failedUrls);
  }

  render() {
    return html`${this.layout ? this.layout() : this.renderBeforeFetch()}`;
  }
}
