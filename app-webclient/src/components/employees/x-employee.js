import { FetchElement } from '../fetch-element';
import { html, css } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';

import '../../app-layout/mdc-dialog.js'
import { URL_EMPLOYEES } from '../../app-layout/menu-contents';
import { postData } from '../helpers.js';
import './x-employee-form.js'

let router = '';
export const setRouter = r => router = r;

class Employee extends FetchElement {
  static get styles() {
    return css([style])
  }

  static get properties() {
    return {
      // device: { type: Object },
      formComplete: { type: Boolean }
    }
  }

  connectedCallback() {
    // TODO(piotrek): fetch real API
    const urlsToFetch = {};
    const employeeId = this.location.params.id;
    if (employeeId) urlsToFetch.employee = HU_SERVER_URL + 'api/employee/read_one.php?id=' + employeeId;
    super.connectedCallback(urlsToFetch);
  }

  promptDelete() {
    const employee = { id: this.location.params.id };

    const dialog = document.createElement('x-dialog');
    dialog.title = 'Löschen';
    dialog.template = html`Sind Sie sicher?`;
    dialog.onYes = () => {
      const DELETE_URL = HU_SERVER_URL + 'api/employee/delete.php';
      postData(DELETE_URL, employee).then(() => router.constructor.go(URL_EMPLOYEES));
    }

    this.renderRoot.appendChild(dialog);
  }

  promptUpdate() {
    const dialog = document.createElement('x-dialog');
    dialog.title = 'Speichern';
    dialog.template = html`Änderungen speichern?`;
    dialog.onYes = () => {
      const UPDATE_URL = HU_SERVER_URL + 'api/employee/update.php';
      postData(UPDATE_URL, this.employee).then(() => router.constructor.go(URL_EMPLOYEES));
    }

    this.renderRoot.appendChild(dialog);
  }

  promptSave() {
    const dialog = document.createElement('x-dialog');
    dialog.title = 'Speichern';
    dialog.template = html`Neuen Eintrag speichern?`;
    dialog.onYes = () => {
      // console.log(employee)
      const CREATE_URL = HU_SERVER_URL + 'api/employee/create.php';
      postData(CREATE_URL, this.employee).then(() => router.constructor.go(URL_EMPLOYEES));
    }

    this.renderRoot.appendChild(dialog);
  }

  renderBeforeFetch() {
    return html`
      <h2>Mitarbeiter</h2>
      Loading...
    `
  }

  renderAfterFetch() {
    if (!this.employee) this.employee = this.fetchedData.employee ? this.fetchedData.employee : {};

    if (this.fetchedData.employee) {
      // editing employee
      return html`
        <button @click=${this.promptDelete} class="mdc-button mdc-button--outlined" style="float:right; border-color:red; color:red; margin-left:16px;">
          <i class="material-icons mdc-button__icon" aria-hidden="true">delete</i>
          <span class="mdc-button__label">Löschen</span>
        </button>
        <button ?disabled=${!this.formComplete} @click=${this.promptUpdate} class="mdc-button mdc-button--raised" style='float:right;'>
          <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
          <span class="mdc-button__label">Speichern</span>
        </button>
        <h2>Mitarbeiter #${this.location.params.id}</h2>
        <x-employee-form .employee=${this.employee} @employee-change=${this.onEmployeeChange}></x-employee-form>
      `
    } else {
      // new employee
      return html`
        <button ?disabled=${!this.formComplete} @click=${this.promptSave} class="mdc-button mdc-button--raised" style='float:right;'>
          <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
          <span class="mdc-button__label">Speichern</span>
        </button>
        <h2>Neuer Mitarbeiter</h2>
        <x-employee-form .employee=${this.employee} @employee-change=${this.onEmployeeChange}></x-employee-form>
      `
    }
  }

  onEmployeeChange(event) {
    // console.log(event)
    this.employee = event.detail.employee; //property is not observed
    this.formComplete = Boolean(this.employee.name && this.employee.email && this.employee.phone); //observed property, change causes rerender
  }

}

customElements.define('x-employee', Employee);
