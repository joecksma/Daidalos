import { html, LitElement } from 'lit-element';

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';

class EmployeeForm extends LitElement {
  static get properties() {
    return {
      employee: { type: Object }
    }
  }

  updated(changedProperties) {
    // console.log(changedProperties);
    this.dispatchEvent(new CustomEvent('employee-change', {
      detail: {
        employee: this.employee
      }
    }));
  }

  firstUpdated() {
    if (this.employee.name) this.renderRoot.getElementById('name').value = this.employee.name;
    if (this.employee.email) this.renderRoot.getElementById('email').value = this.employee.email;
    if (this.employee.phone) this.renderRoot.getElementById('phone').value = this.employee.phone;
  }

  onNameInput(event) {
    this.employee = {
      ...this.employee,
      name: event.composedPath()[0].value
    }
  }

  onEmailInput(event) {
    this.employee = {
      ...this.employee,
      email: event.composedPath()[0].value
    }
  }


  onPhoneInput(event) {
    this.employee = {
      ...this.employee,
      phone: event.composedPath()[0].value
    }
  }

  render() {
    return html`
      <vaadin-form-layout>
        <vaadin-text-field id='name' label="Name" @input=${this.onNameInput}></vaadin-text-field>
        <vaadin-text-field id='email' label="Email" @input=${this.onEmailInput}></vaadin-text-field>
        <vaadin-text-field id='phone' label="Telefon" @input=${this.onPhoneInput}></vaadin-text-field>
      </vaadin-form-layout>
    `
  }

}

customElements.define('x-employee-form', EmployeeForm);
