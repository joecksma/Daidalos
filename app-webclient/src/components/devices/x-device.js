import { FetchElement } from '../fetch-element';
import { html, css } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '../../app-layout/mdc-dialog.js'
import { URL_DEVICES } from '../../app-layout/menu-contents';

import './x-device-form.js'
import { getEmployeesComboFormat, getDeviceWithFlattenedSteps } from '../helpers.js';

let router = '';
export const setRouter = r => router = r;

class Device extends FetchElement {
  static get styles() {
    return css([style]);
  }

  static get properties() {
    return {
      // device: { type: Object },
      formComplete: { type: Boolean }
    }
  }

  connectedCallback() {
    const urlsToFetch = { employees: APP_HOST + 'api/employees.json' }

    const deviceId = this.location.params.id;
    if (deviceId) {
      // TODO(piotrek): fetch real API
      urlsToFetch.device = APP_HOST + 'api/devices/' + deviceId + '.json';
    }
    super.connectedCallback(urlsToFetch);
  }

  promptDelete() {
    const dialog = document.createElement('x-dialog');
    dialog.title = 'Löschen';
    dialog.template = html`Sind Sie sicher?`;
    dialog.onYes = () => {
      // TODO(piotrek): HTTP DELETE request
      console.warn('TODO(piotrek): deleting employee entry', this.location.params.id);

      // navigate back after deletion
      router.constructor.go(URL_DEVICES)
    }

    this.renderRoot.appendChild(dialog);
  }

  promptSave() {
    console.log(this.device)
  }

  renderBeforeFetch() {
    return html`
      <h2>Anlage</h2>
      Loading...
    `
  }

  renderAfterFetch() {
    if (!this.device) this.device = this.fetchedData.device ? getDeviceWithFlattenedSteps(this.fetchedData.device) : { steps: [] };
    if (!this.employees) this.employees = getEmployeesComboFormat(this.fetchedData.employees);
    // console.log('x-device render', this.device)

    if (this.fetchedData.device) {
      // editing device
      return html`
        <button @click=${this.promptDelete} class="mdc-button mdc-button--outlined" style="float:right; border-color:red; color:red; margin-left:16px;">
          <i class="material-icons mdc-button__icon" aria-hidden="true">delete</i>
          <span class="mdc-button__label">Löschen</span>
        </button>
        <button ?disabled=${!this.formComplete} @click=${this.promptUpdate} class="mdc-button mdc-button--raised" style='float:right;'>
          <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
          <span class="mdc-button__label">Speichern</span>
        </button>
        <h2>Anlage #${this.fetchedData.device.id}</h2>
        <x-device-form .device=${this.device} .employees=${this.employees} @device-change=${this.onDeviceChange}></x-device-form>
      `

    } else {
      // new device
      return html`
        <button ?disabled=${!this.formComplete} @click=${this.promptSave} class="mdc-button mdc-button--raised" style='float:right;'>
          <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
          <span class="mdc-button__label">Speichern</span>
        </button>
        <h2>Neue Anlage</h2>
        <x-device-form .device=${this.device} .employees=${this.employees} @device-change=${this.onDeviceChange}></x-device-form>
      `
    }
  }

  onDeviceChange(event) {
    // console.log('onDeviceChange', event.detail.device);
    this.device = event.detail.device; //property is not observed
    this.formComplete = Boolean(this.device.name && this.device.address && this.device.employee && this.device.steps.length); //observed property, change causes rerender
  }

}
customElements.define('x-device', Device);
