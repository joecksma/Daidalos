import { FetchElement } from '../fetch-element';
import { html, css } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-filter-column';
import { URL_DEVICES } from '../../app-layout/menu-contents';

let router = '';
export const setRouter = r => router = r;

class Devices extends FetchElement {
  static get styles() {
    return css([style]);
  }

  connectedCallback() {
    // TODO(piotrek): fetch real API
    const urls = { devices: APP_HOST + 'api/devices.json' };
    super.connectedCallback(urls);
  }

  renderBeforeFetch() {
    return html`
      <h2>Anlagen</h2>
      Loading...
    `
  }

  renderAfterFetch(data) {
    return html`
      <a href=${URL_DEVICES + '/new'} class="mdc-button mdc-button--raised" style='float:right;'>
        <i class="material-icons mdc-button__icon" aria-hidden="true">add</i>
        <span class="mdc-button__label">Hinzufügen</span>
      </a>
      <h2>Anlagen</h2>
      <vaadin-grid .items=${data.devices} @active-item-changed=${this.onActiveItemChanged}>
        <vaadin-grid-filter-column flex-grow="0" path="id" header="#" width="160px"></vaadin-grid-filter-column>
        <vaadin-grid-filter-column flex-grow="1" path="name" header="Anlage"></vaadin-grid-filter-column>
        <vaadin-grid-filter-column flex-grow="1" path="address" header="Ort"></vaadin-grid-filter-column>
      </vaadin-grid>
    `
  }

  onActiveItemChanged(event) {
    if (event.detail.value) {
      router.constructor.go(URL_DEVICES + '/' + event.detail.value.id)
    }
  }

}
customElements.define('x-devices', Devices);
