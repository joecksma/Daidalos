import { html, css, LitElement } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';

import { formLayoutResponsiveSteps } from '../helpers.js';
import { STEP_TYPES } from '../contracts/step-types';
import { render } from 'lit-html';

class DeviceForm extends LitElement {
  static get styles() {
    return css([style]);
  }

  static get properties() {
    return {
      device: { type: Object }
    }
  }

  updated(changedProperties) {
    // console.log(changedProperties);
    this.dispatchEvent(new CustomEvent('device-change', {
      detail: {
        device: this.device
      }
    }));
  }

  firstUpdated() {
    if (this.device.name) this.renderRoot.getElementById('name').value = this.device.name;
    if (this.device.address) this.renderRoot.getElementById('address').value = this.device.address;
    if (this.device.employee) this.renderRoot.getElementById('employee').value = this.device.employee.id;
  }

  onNameInput(event) {
    this.device = {
      ...this.device,
      name: event.composedPath()[0].value
    }
  }

  onAddressInput(event) {
    this.device = {
      ...this.device,
      address: event.composedPath()[0].value
    }
  }

  onEmployeeSelectedItemChange(event) {
    this.device = {
      ...this.device,
      employee: event.detail.value
    }
  }

  render() {
    // console.log('x-device-form render!', this.device)

    const moveUpStepButtonRenderer = (root, col, rowData) => {
      render((rowData.index > 0) ? html`
        <button @click=${()=> this.moveUpStep(rowData.index)} class="mdc-button" style="height:30px; width:30px">
          <i class="material-icons mdc-button__icon" aria-hidden="true" style="margin-right:0;">arrow_upward</i>
        </button>`: ``, root);
    }

    const moveDownStepButtonRenderer = (root, col, rowData) => {
      render((rowData.index < this.device.steps.length - 1) ? html`
        <button @click=${()=> this.moveDownStep(rowData.index)} class="mdc-button" style="">
          <i class="material-icons mdc-button__icon" aria-hidden="true" style="margin-right:0;">arrow_downward</i>
        </button>` : ``, root);
    }

    const removeStepButtonRenderer = (root, col, rowData) => {
      render(html`
        <button @click=${()=> this.removeStep(rowData)} class="mdc-button" style="border-color:red; color:red;">
          <i class="material-icons mdc-button__icon" aria-hidden="true" style="margin-right:0;">delete</i>
        </button>`, root)
    }

    const stepRenderer = (root, col, rowData) => {
      switch (rowData.item.type) {
        case STEP_TYPES.SCAN_CONFIRMATION:
          return render(scanConfirmationStepRenderer(rowData.item), root);
        case STEP_TYPES.INFO_PDF:
          return render(infoPdfStepStepRenderer(rowData.item), root);
        case STEP_TYPES.INFO_TEXT:
          return render(infoTextStepRenderer(rowData.item), root);
        case STEP_TYPES.CHECK:
          return render(checkStepRenderer(rowData.item), root);
        case STEP_TYPES.FEEDBACK:
          return render(feedbackStepRenderer(rowData.item), root);
      }
    }

    const scanConfirmationStepRenderer = () => html`Scan confirmation`;

    const infoPdfStepStepRenderer = item =>
      html`
        <vaadin-form-layout>
          <vaadin-text-field placeholder="Titel der Datei" value=${item.title} @input=${e=> this.onStepTitleInput(e, item)}></vaadin-text-field>
          <span>
            ${item.pdf ? html`
            <button class='mdc-button' @click=${()=> {
            // TODO(piotrek): we could download/save this base64 to temp and then open real FileURL from disk
            const win = window.open();
            const iframe = document.createElement('iframe');
            iframe.src = item.pdf;
            iframe.frameBorder = '0';
            iframe.style = 'position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%';
            win.document.body.appendChild(iframe);
            }}>PDF Datei anschauen</button>
            <button @click=${()=> this.removeFile(item)} class="mdc-button" style="height:30px; border-color:red; color:red;">
              <i class="material-icons mdc-button__icon" aria-hidden="true" style="margin-right:0;">delete</i>
            </button>
        
            ` : html`
            <input id='file-input' accept=".pdf" type="file" @change=${e=> this.onFileInputChange(e, item)} hidden>
            <button class='mdc-button' @click=${()=> this.renderRoot.getElementById('file-input').click()}>PDF Datei hochladen</button>`}
          </span>
        </vaadin-form-layout>
      `;

    const infoTextStepRenderer = item =>
      html`
        <vaadin-form-layout>
          <vaadin-text-field placeholder="Titel der Nachricht" value=${item.title} @input=${e=> this.onStepTitleInput(e,
            item)}></vaadin-text-field>
          <vaadin-text-field placeholder="Nachricht an den Auftragnehmer" value=${item.description} @input=${e=>
            this.onStepDescriptionInput(e, item)}></vaadin-text-field>
        </vaadin-form-layout>
      `;

    const checkStepRenderer = item =>
      html`
        <vaadin-form-layout>
          <vaadin-text-field placeholder="Titel der Aufgabe" value=${item.title} @input=${e=> this.onStepTitleInput(e, item)}></vaadin-text-field>
          <vaadin-text-field placeholder="Beschreibung der Aufgabe" value=${item.description} @input=${e=>
            this.onStepDescriptionInput(e, item)}></vaadin-text-field>
        </vaadin-form-layout>
      `;

    const feedbackStepRenderer = item =>
      html`
        <vaadin-form-layout>
          <vaadin-text-field placeholder="Titel des Feedbacks" value=${item.title} @input=${e=> this.onStepTitleInput(e,
            item)}></vaadin-text-field>
          <vaadin-text-field placeholder="Beschreibung des Feedbacks" value=${item.description} @input=${e=>
            this.onStepDescriptionInput(e, item)}></vaadin-text-field>
        </vaadin-form-layout>
      `;


    return html`
      <vaadin-form-layout .responsiveSteps=${formLayoutResponsiveSteps}>
        <vaadin-text-field id='name' label='Name' @input=${this.onNameInput}></vaadin-text-field>
        <vaadin-text-field id='address' label='Adresse' @input=${this.onAddressInput}></vaadin-text-field>
        <vaadin-combo-box id='employee' label="Mitarbeiter vor Ort" .items=${this.employees} @selected-item-changed=${this.onEmployeeSelectedItemChange}></vaadin-combo-box>
      
        <vaadin-grid id='grid' .items=${this.device.steps} style='margin:16px 0 8px; max-height:calc(100vh - 400px)' colspan='3'>
          <vaadin-grid-column flex-grow="1" header="Schritt" .renderer=${stepRenderer}></vaadin-grid-column>
          <vaadin-grid-column flex-grow='0' width='80px' .renderer=${moveUpStepButtonRenderer}></vaadin-grid-column>
          <vaadin-grid-column flex-grow='0' width='80px' .renderer=${moveDownStepButtonRenderer}></vaadin-grid-column>
          <vaadin-grid-column flex-grow='0' width='99px' .renderer=${removeStepButtonRenderer}></vaadin-grid-column>
        </vaadin-grid>
      </vaadin-form-layout>
      
      Hinzufügen:
      <button class='mdc-button' @click=${()=> this.addStep(STEP_TYPES.SCAN_CONFIRMATION)}>Scan</button>
      <button class='mdc-button' @click=${()=> this.addStep(STEP_TYPES.INFO_PDF)}>PDF Datei</button>
      <button class='mdc-button' @click=${()=> this.addStep(STEP_TYPES.INFO_TEXT)}>Informationstext</button>
      <button class='mdc-button' @click=${()=> this.addStep(STEP_TYPES.CHECK)}>Aufgabe</button>
      <button class='mdc-button' @click=${()=> this.addStep(STEP_TYPES.FEEDBACK)}>Feedback</button>
      <button class='mdc-button' style='float:right; display:none' @click=${() => console.log(this.device)}>console.log device</button>
    `
  }

  onStepTitleInput(e, item) {
    const updatedStep = { ...item, desctitleription: e.composedPath()[0].value };
    this.device = {
      ...this.device,
      steps: this.device.steps.map(oldStep => oldStep == item ? updatedStep : oldStep)
    }
  }

  onStepDescriptionInput(e, item) {
    const updatedStep = { ...item, description: e.composedPath()[0].value };
    this.device = {
      ...this.device,
      steps: this.device.steps.map(oldStep => oldStep == item ? updatedStep : oldStep)
    }
  }

  onFileInputChange(e, item) {
    const reader = new FileReader();
    reader.onload = loadEvent => {
      const updatedStep = { ...item, pdf: loadEvent.target.result }
      this.device = {
        ...this.device,
        steps: this.device.steps.map(oldStep => oldStep == item ? updatedStep : oldStep)
      }
    }

    reader.readAsDataURL(e.composedPath()[0].files[0]);
  }

  removeFile(item) {
    const updatedStep = { ...item, pdf: undefined };
    this.device = {
      ...this.device,
      steps: this.device.steps.map(oldStep => oldStep == item ? updatedStep : oldStep)
    }
  }

  addStep(stepType) {
    let newStep = { type: stepType };
    if (stepType == STEP_TYPES.INFO_TEXT || stepType == STEP_TYPES.CHECK || stepType == STEP_TYPES.FEEDBACK) {
      newStep.title = '';
      newStep.description = '';
      newStep.required = true;
    }

    if (STEP_TYPES.INFO_PDF) {
      newStep.title = '';
      newStep.required = true;
    }

    this.device = {
      ...this.device,
      steps: this.device.steps.concat(newStep)
    }
  }

  moveUpStep(index) {
    [this.device.steps[index], this.device.steps[index - 1]] = [this.device.steps[index - 1], this.device.steps[index]];
    this.renderRoot.getElementById('grid').clearCache();
    this.device = { ...this.device } //causes rerender
  }

  moveDownStep(index) {
    [this.device.steps[index], this.device.steps[index + 1]] = [this.device.steps[index + 1], this.device.steps[index]];
    this.renderRoot.getElementById('grid').clearCache();
    this.device = { ...this.device } //causes rerender
  }

  removeStep(rowData) {
    this.device = {
      ...this.device,
      steps: this.device.steps.filter(x => x != rowData.item)
    }
  }

}
customElements.define('x-device-form', DeviceForm);
