import { LitElement, html } from 'lit-element';
import { Router } from '@vaadin/router'
import '../app-layout/app-layout'
import { URL_CONTRACTS, URL_NEW_CONTRACT, URL_DEVICES, URL_COMPANIES, URL_EMPLOYEES, URL_DUPLICATE_CONTRACT } from '../app-layout/menu-contents';

class App extends LitElement {
	static get properties() {
		return {
			_firstPageLoad: { type: Boolean },
			_selectedMenuItem: { type: String },
		};
	}

	constructor() {
		super();
		this._firstPageLoad = true;
	}

	render() {
		return html`
			<app-layout .selectedMenuItemUrl=${this._selectedMenuItem}>
				<div id="router-host" slot="pages"></div>
			</app-layout>
    `;
	}

	setMenuItem(url) {
		// console.log('setmenuitem called', url)
		this._selectedMenuItem = url;
	}

	firstUpdated() {
		const host = this.shadowRoot.querySelector('#router-host');
		const router = new Router(host);

		router.setMenuItem = url => this._selectedMenuItem = url;
		router.setRoutes([
			//REDIRECTS
			{ path: '/', redirect: URL_CONTRACTS },

			//ROUTES
			{
				path: URL_CONTRACTS, component: 'x-current-contracts', action: () => {
					import(/* webpackChunkName: 'contracts' */'./contracts/x-contracts.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_CONTRACTS);
				},
			},
			{
				path: URL_NEW_CONTRACT, component: 'x-new-contract', action: () => {
					import(/* webpackChunkName: 'new-contract' */'./contracts/x-new-contract.js');
					this.setMenuItem(URL_NEW_CONTRACT);
				},
			},
			{
				path: URL_DUPLICATE_CONTRACT + '/:contractIdToDuplicate', component: 'x-new-contract', action: () => {
					import(/* webpackChunkName: 'new-contract' */'./contracts/x-new-contract.js');
					this.setMenuItem(URL_NEW_CONTRACT);
				},
			},
			{
				path: URL_CONTRACTS + '/:id', component: 'x-contract', action: () => {
					import(/* webpackChunkName: 'contract' */'./contracts/x-contract.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_CONTRACTS);
				},
			},
			{
				path: URL_CONTRACTS + '/:id/device/:deviceId', component: 'x-contract-device', action: () => {
					import(/* webpackChunkName: 'contract-device' */'./contracts/x-contract-device.js');
					this.setMenuItem(URL_CONTRACTS);
				},
			},
			{
				path: URL_DEVICES, component: 'x-devices', action: () => {
					import(/* webpackChunkName: 'devices' */'./devices/x-devices.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_DEVICES);
				},
			},
			{
				path: URL_DEVICES + '/new', component: 'x-device', action: () => {
					import(/* webpackChunkName: 'device' */'./devices/x-device.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_DEVICES);
				},
			},
			{
				path: URL_DEVICES + '/:id', component: 'x-device', action: () => {
					import(/* webpackChunkName: 'device' */'./devices/x-device.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_DEVICES);
				},
			},
			{
				path: URL_COMPANIES, component: 'x-companies', action: () => {
					import(/* webpackChunkName: 'companies' */'./companies/x-companies.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_COMPANIES);
				},
			},
			{
				path: URL_COMPANIES + '/new', component: 'x-company-new', action: () => {
					import(/* webpackChunkName: 'company' */'./companies/x-company-new.js');
					this.setMenuItem(URL_COMPANIES);
				},
			},
			{
				path: URL_COMPANIES + '/:id', component: 'x-company', action: () => {
					import(/* webpackChunkName: 'company' */'./companies/x-company.js');
					this.setMenuItem(URL_COMPANIES);
				},
			},
			{
				path: URL_EMPLOYEES, component: 'x-employees', action: () => {
					import(/* webpackChunkName: 'employees' */'./employees/x-employees.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_EMPLOYEES);
				},
			},
			{
				path: URL_EMPLOYEES + '/new', component: 'x-employee', action: () => {
					import(/* webpackChunkName: 'employee' */'./employees/x-employee.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_EMPLOYEES);
				},
			},
			{
				path: URL_EMPLOYEES + '/:id', component: 'x-employee', action: () => {
					import(/* webpackChunkName: 'employee' */'./employees/x-employee.js').then(m => m.setRouter(router));
					this.setMenuItem(URL_EMPLOYEES);
				},
			},
			{
				path: '(.*)', component: 'x-404', action: () => {
					import(/* webpackChunkName: '404' */'./x-404.js');
				}
			},
		]);
	}

}

customElements.define('x-app', App);
