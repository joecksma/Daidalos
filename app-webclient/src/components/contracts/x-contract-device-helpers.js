import { render, html } from "lit-html";

export const requiredRenderer = (root, col, rowData) => {
  if (!root.firstElementChild) {
    render(rowData.item.required ? html`<i class="material-icons">done</i>` : '', root)
  }
}

export const doneRenderer = (root, col, rowData) => {
  if (!root.firstElementChild) {
    render(rowData.item.done ? html`<i class="material-icons">done</i>` : '', root)
  }
}

export const detailsRenderer = (root, grid, rowData) => {
  if (!root.firstElementChild) {
    switch (rowData.item.type) {
      case 'SCAN_CONFIRMATION':
        return render(scanConfirmationDetails(rowData.item), root);
      case 'INFO_PDF':
        return render(infoPdfDetails(rowData.item), root);
      case 'INFO_TEXT':
        return render(infoTextDetails(rowData.item), root);
      case 'FEEDBACK':
        return render(feedbackDetails(rowData.item), root);
      case 'PHOTOS_INPUT':
        return render(photosInputDetails(rowData.item), root);
      case 'ACTIONS':
        return render(actionsDetails(rowData.item), root);
    }
  }
}

const scanConfirmationDetails = item => item.done ?
  html`<p><b>Status: </b>Code-Vergleich war erfolgreich</p>` :
  html`<p><b>Status: </b>Noch nicht gescannt</p>`;


const infoPdfDetails = item =>
  html`<p><b>Flyer: </b><a href="${item.data.url}" target='_blank'>${item.data.url}</a></p>`;

const infoTextDetails = item =>
  html`<p><b>Info text: </b>${item.data.text}</p>`;

const photosInputDetails = item => item.done ?
  html`
    <p><b>Angehängte Fotos: </b></p>
    <ul>
      ${item.data.map(photo => html`<li>
        <a href=${photo.url} target="_blank">Foto am ${new Date(photo.timestamp).toLocaleString()}</a>
      </li>`)}
    </ul>
  `:
  html`<p><b>Status: </b>Der Benutzer hat jetzt die Möglichkeit, Fotos aufzunehmen</p>`;

const feedbackDetails = item => item.done ?
  html`<p><b>Kommentar: </b>${item.data.text}</p>` :
  html`<p><b>Status: </b>Der Benutzer hat jetzt die Möglichkeit, einen Kommentar hinzuzufügen</p>`;

const actionsDetails = item => html`
  <p><b>Wartungsschritte: </b></p>
  <ul>
    ${item.data.map(step => html`
    <li><b>${step.title}: </b>${step.description}</li>
    `)}
  </ul>
`
