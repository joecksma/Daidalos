import { FetchElement } from '../fetch-element.js';
import { html, css } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-filter-column';
import '../../app-layout/mdc-dialog.js'

import { URL_CONTRACTS, URL_DUPLICATE_CONTRACT } from '../../app-layout/menu-contents';
import { format } from 'date-fns/esm';
import { de as deLocale } from 'date-fns/esm/locale';

let router = '';
export const setRouter = r => router = r;

class Contract extends FetchElement {
  static get styles() {
    return [
      css([style]),
      css`
      vaadin-grid {
        height: calc(100vh - 280px);
      }`
    ];
  }

  connectedCallback() {
    // TODO(piotrek): fetch real API
    // const contractId = this.location.params.id;
    const urls = { contract: APP_HOST + 'api/contract.json' }
    super.connectedCallback(urls);
  }

  promptDelete() {
    const dialog = document.createElement('x-dialog');
    dialog.title = 'Löschen';
    dialog.template = html`Sind Sie sicher?`;
    dialog.onYes = () => {
      // TODO(piotrek): HTTP DELETE request
      console.warn('TODO(piotrek): deleting employee entry', this.location.params.id);

      // navigate back after deletion
      location.href = URL_CONTRACTS;
    }

    this.renderRoot.appendChild(dialog);
  }

  renderBeforeFetch() {
    return html`
      <h2>Wartung #${this.location.params.id}</h2>
      Loading...
    `
  }

  renderAfterFetch(data) {
    return html`
      <button @click=${this.promptDelete} class="mdc-button mdc-button--outlined" style="float:right; border-color:red; color:red; margin-left:16px;">
        <i class="material-icons mdc-button__icon" aria-hidden="true">delete</i>
        <span class="mdc-button__label">Wartung löschen</span>
      </button>
      <button @click=${() => router.constructor.go(URL_DUPLICATE_CONTRACT + '/' + this.location.params.id)} class="mdc-button mdc-button--raised" style="float:right;">
        <i class="material-icons mdc-button__icon" aria-hidden="true">update</i>
        <span class="mdc-button__label">Duplizieren</span>
      </button>
      <h2>Wartung #${this.location.params.id} - ${data.contract.name}</h2>
      <p><b>Firma: </b><a href='/companies/${data.contract.company.id}'>${data.contract.company.name}</a></p>
      <p><b>Wann: </b>${this.formatDate(data.contract.startTime)} - ${this.formatDate(data.contract.endTime)}</p>
      <p><b>Token: </b>${data.contract.token}</p>
      
      <vaadin-grid .items=${data.contract.devices} @active-item-changed=${this.onActiveItemChanged}>
        <vaadin-grid-filter-column flex-grow="0" path="id" header="#" width="160px"></vaadin-grid-filter-column>
        <vaadin-grid-filter-column flex-grow="1" path="name" header="Anlage"></vaadin-grid-filter-column>
        <vaadin-grid-filter-column flex-grow="1" path="address" header="Ort"></vaadin-grid-filter-column>
      </vaadin-grid>
      
      <p></p>
    `
  }

  onActiveItemChanged(event) {
    if (event.detail.value) {
      router.constructor.go(URL_CONTRACTS + '/' + this.location.params.id + '/device/' + event.detail.value.id)
    }
  }

  formatDate(date) {
    return format(new Date(date), 'd MMMM YYY h:mm', { locale: deLocale })
  }

}

customElements.define('x-contract', Contract);
