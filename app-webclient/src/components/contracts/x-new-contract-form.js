import { LitElement, html, css } from 'lit-element';
import { render } from 'lit-html';
import style from '../common.scss'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';

import './x-new-contract-device-modal.js'
import { datePickerI18n } from './x-new-contract-form-helpers';

import '../../app-layout/mdc-dialog.js'
import { URL_CONTRACTS } from '../../app-layout/menu-contents';
import { format, parse } from 'date-fns/esm';
import { getCompaniesComboFormat, getDevicesComboFormat, getEmployeesComboFormat } from '../helpers.js';

class NewContractForm extends LitElement {
  static get styles() {
    return css([style]);
  }

  static get properties() {
    return {
      contract: { type: Object },
    };
  }

  connectedCallback() {
    // is called before first render
    super.connectedCallback();
    this.contract = this.fetchedData.contract ? this.deleteStepsData(this.fetchedData.contract) : { devices: [] };

    this.companies = getCompaniesComboFormat(this.fetchedData.companies);
    this.devices = getDevicesComboFormat(this.fetchedData.devices);
    this.employees = getEmployeesComboFormat(this.fetchedData.employees);
  }

  firstUpdated() {
    // is called after first render
    if (this.fetchedData.contract) {
      this.renderRoot.getElementById('name').value = this.contract.name;
      this.renderRoot.getElementById('company').value = this.contract.company.id;
      this.renderRoot.getElementById('startTime').value = this.formatISOStringToPicker(this.contract.startTime);
      this.renderRoot.getElementById('endTime').value = this.formatISOStringToPicker(this.contract.endTime);
    }
  }

  deleteStepsData(contract) {
    return {
      ...contract,
      devices: contract.devices.map(device => {
        return {
          ...device,
          steps: device.steps.map(step => {
            return {
              ...step,
              data: {}
            }
          })
        }
      })
    }
  };

  promptSave() {
    const dialog = document.createElement('x-dialog');
    dialog.title = 'Speichern';
    dialog.template = 'Sind Sie sicher?';
    dialog.onYes = () => {
      // TODO(piotrek): HTTP POST call
      console.warn('TODO(piotrek): HTTP POST call', this.contract);
      console.warn('TODO(piotrek): router.go() to this contract after successfull creation');
      location.href = URL_CONTRACTS;
    }
    this.renderRoot.appendChild(dialog);
  }

  onNameInput(event) {
    this.contract = {
      ...this.contract,
      name: event.composedPath()[0].value
    }
  }

  onCompanySelectedItemChanged(event) {
    this.contract = {
      ...this.contract,
      company: event.detail.value
    }
  }

  onStartTimeChanged(event) {
    this.contract = {
      ...this.contract,
      startTime: this.parsePickerToISOString(event.composedPath()[0].value)
    }
  }

  onEndTimeChanged(event) {
    this.contract = {
      ...this.contract,
      endTime: this.parsePickerToISOString(event.composedPath()[0].value)
    }
  }

  onDevicesSelectedItemChanged(event) {
    const selectedDevice = event.detail.value;
    if (selectedDevice) {
      // console.log('selectedDevice:', selectedDevice)
      const deviceModal = document.createElement('x-new-contract-device-modal');
      deviceModal.deviceId = selectedDevice.id;
      deviceModal.employees = this.employees;
      deviceModal.onYes = deviceAfterChanges => {
        this.contract = {
          ...this.contract,
          devices: [...this.contract.devices, deviceAfterChanges]
        };
        // force devicesCombo to call dataProvider callback on next open
        this.renderRoot.getElementById('devices').clearCache();
        this.renderRoot.getElementById('grid').clearCache();
      }
      this.renderRoot.appendChild(deviceModal);
      this.renderRoot.getElementById('devices').value = '';
    }
  }

  moveUpDevice(index) {
    [this.contract.devices[index], this.contract.devices[index - 1]] = [this.contract.devices[index - 1], this.contract.devices[index]];
    this.renderRoot.getElementById('grid').clearCache();
  }

  moveDownDevice(index) {
    [this.contract.devices[index], this.contract.devices[index + 1]] = [this.contract.devices[index + 1], this.contract.devices[index]];
    this.renderRoot.getElementById('grid').clearCache();
  }

  removeDevice(rowData) {
    this.contract = {
      ...this.contract,
      devices: this.contract.devices.filter(x => x != rowData.item)
    }
    this.renderRoot.getElementById('devices').clearCache();
  }

  render() {
    // console.log('render');
    const moveUpDeviceButtonRenderer = (root, col, rowData) => {
      render((rowData.index > 0) ? html`
        <button @click=${()=> this.moveUpDevice(rowData.index)} class="mdc-button" style="height:30px">
          <i class="material-icons mdc-button__icon" aria-hidden="true" style="margin-right:0;">arrow_upward</i>
        </button>`: ``, root);
    }

    const moveDownDeviceButtonRenderer = (root, col, rowData) => {
      render((rowData.index < this.contract.devices.length - 1) ? html`
        <button @click=${()=> this.moveDownDevice(rowData.index)} class="mdc-button" style="height:30px">
          <i class="material-icons mdc-button__icon" aria-hidden="true" style="margin-right:0;">arrow_downward</i>
        </button>` : ``, root);
    }

    const removeDeviceButtonRenderer = (root, col, rowData) => {
      render(html`
        <button @click=${()=> this.removeDevice(rowData)} class="mdc-button" style="height:30px; border-color:red; color:red;">
          <i class="material-icons mdc-button__icon" aria-hidden="true" style="margin-right:0;">delete</i>
        </button>
      `, root)
    }

    const devicesDataProvider = (params, callback) => {
      const filteredDevices =
        this.devices
          .filter(device => !this.contract.devices.map(contractDevice => contractDevice.id).includes(device.id))
          .filter(device => device.name.includes(params.filter) || device.id.includes(params.filter));
      callback(filteredDevices, filteredDevices.length)
    }

    return html`
      <button ?disabled=${!this.isSavePossible()} @click=${this.promptSave} class="mdc-button mdc-button--raised" style='float:right;'>
        <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
        <span class="mdc-button__label">Speichern</span>
      </button>
      <h2>Neue Wartung</h2>
      
      <vaadin-form-layout>
        <vaadin-text-field id='name' label="Name" @input=${this.onNameInput}></vaadin-text-field>
        <vaadin-combo-box id='company' label="Auftragnehmer" .items=${this.companies} @selected-item-changed=${this.onCompanySelectedItemChanged}></vaadin-combo-box>
        <vaadin-date-picker id='startTime' label="Frühestes Startdatum" @change=${this.onStartTimeChanged} .i18n=${datePickerI18n}></vaadin-date-picker>
        <vaadin-date-picker id='endTime' label="Spätestes Enddatum" @change=${this.onEndTimeChanged} .i18n=${datePickerI18n}></vaadin-date-picker>
        <vaadin-combo-box id='devices' label="Anlage hinzufügen" .dataProvider=${devicesDataProvider} @selected-item-changed=${this.onDevicesSelectedItemChanged}></vaadin-combo-box>
        <button class='mdc-button' style='display:none' @click=${()=> console.log(this.contract, 'isSavePossible:', this.isSavePossible())}>console.log contract</button>
      
        <vaadin-grid id='grid' style="height: calc(100vh - 400px); margin-top:16px" colspan="2" .items=${this.contract.devices}
          @active-item-changed=${this.onGridActiveItemChanged}>
          <vaadin-grid-column resizable flex-grow='0' header="#" path='id' width="160px"></vaadin-grid-column>
          <vaadin-grid-column flex-grow='1' header="Anlagen in der Wartung" path='name'></vaadin-grid-column>
          <vaadin-grid-column flex-grow='0' width='80px' .renderer=${moveUpDeviceButtonRenderer}></vaadin-grid-column>
          <vaadin-grid-column flex-grow='0' width='80px' .renderer=${moveDownDeviceButtonRenderer}></vaadin-grid-column>
          <vaadin-grid-column flex-grow='0' width='100px' .renderer=${removeDeviceButtonRenderer}></vaadin-grid-column>
        </vaadin-grid>
      
      </vaadin-form-layout>
    `;
  }

  onGridActiveItemChanged(event) {
    this.renderRoot.getElementById('grid').activeItem = undefined;
    const selectedDevice = event.detail.value;

    if (selectedDevice) {
      const deviceIndex = this.contract.devices.findIndex(x => x == selectedDevice);

      const deviceModalForm = document.createElement('x-new-contract-device-modal-form');
      deviceModalForm.deviceData = selectedDevice;
      deviceModalForm.employees = this.employees;
      deviceModalForm.onYes = deviceAfterChanges => {
        this.contract.devices[deviceIndex] = deviceAfterChanges;
        this.renderRoot.getElementById('grid').clearCache();
      }
      this.renderRoot.appendChild(deviceModalForm);
    }
  }

  formatISOStringToPicker(isoDateString) {
    // comes as "2019-01-10T08:00:00.000Z" usually from datebase
    return format(new Date(isoDateString), 'yyyy-MM-dd');
  }

  parsePickerToISOString(pickerDate) {
    // comes as "2019-01-01" ... "2019-12-31" from datePicker
    let date = new Date(parse(pickerDate, 'yyyy-MM-dd', new Date()))
    return date.toISOString();
  }

  isSavePossible() {
    return Boolean(this.contract.name && this.contract.company && this.contract.startTime && this.contract.endTime && this.contract.devices.length);
  }

}
customElements.define('x-new-contract-form', NewContractForm);
