import { DialogElement } from '../../app-layout/mdc-dialog.js';
import { html } from 'lit-element';
import { getDeviceWithFlattenedSteps } from '../helpers.js';

import '../devices/x-device-form.js'

class NewContractDeviceModalForm extends DialogElement {
  static get properties() {
    return {
      // device: { type: Object },
      formComplete: { type: Boolean }
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.device = getDeviceWithFlattenedSteps(this.deviceData);
  }

  render() {
    return html`
      <div class="mdc-dialog mdc-dialog--open" role="dialog" aria-modal="true" aria-labelledby="mdc-dialog-with-list-label"
        aria-describedby="mdc-dialog-with-list-description">
        <div class="mdc-dialog__scrim"></div>
        <div class="mdc-dialog__container">
          <div class="mdc-dialog__surface" style='max-width:calc(100vw - 32px); width:1400px;'>
            <h2 id="mdc-dialog-with-list-label" class="mdc-dialog__title">${this.device.name} #${this.device.id}</h2>
            <section id="mdc-dialog-with-list-description" class="mdc-dialog__content">
              <x-device-form .device=${this.device} .employees=${this.employees} @device-change=${this.onDeviceChange}></x-device-form>      
            </section>
            <footer class="mdc-dialog__actions">
              <button class="mdc-button" data-mdc-dialog-action="cancel">Abbrechen</button>
              <button ?disabled=${!this.formComplete} @click=${() => this.onYes(this.device)} class="mdc-button mdc-button--raised" style='margin-left: 16px'
                data-mdc-dialog-action="yes">Ja</button>
            </footer>
          </div>
        </div>
      </div>
    `;
  }
  
  onDeviceChange(event) {
    // console.log('onDeviceChange', event.detail.device);
    this.device = event.detail.device; //property is not observed
    this.formComplete = Boolean(this.device.name && this.device.address && this.device.employee && this.device.steps.length); //observed property, change causes rerender
    ;
  }

}
customElements.define('x-new-contract-device-modal-form', NewContractDeviceModalForm);
