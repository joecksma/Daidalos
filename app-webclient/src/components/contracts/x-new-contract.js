import { FetchElement } from '../fetch-element.js';
import { html } from 'lit-element';
import './x-new-contract-form.js'

class NewContract extends FetchElement {
  connectedCallback() {
    let urlsToFetch = {
      companies: APP_HOST + 'api/companies.json',
      devices: APP_HOST + 'api/devices.json',
      employees: APP_HOST + 'api/employees.json',
    };
    
    const contractIdToDuplicate = this.location.params.contractIdToDuplicate;    
    if (contractIdToDuplicate) {
      urlsToFetch.contract = APP_HOST + 'api/contract.json';
    }

    super.connectedCallback(urlsToFetch);
  }

  renderAfterFetch() {
    return html`
      <x-new-contract-form .fetchedData=${this.fetchedData}></x-new-contract-form>
    `;
  }

}
customElements.define('x-new-contract', NewContract);
