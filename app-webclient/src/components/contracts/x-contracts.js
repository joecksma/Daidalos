import { FetchElement } from '../fetch-element.js';
import { html, css } from 'lit-element';
import { render } from 'lit-html';
import style from '../common.scss'

import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-filter-column';

import { URL_CONTRACTS } from '../../app-layout/menu-contents';
import { format } from 'date-fns/esm';
import { de as deLocale } from 'date-fns/esm/locale';

let router = '';
export const setRouter = r => router = r;

class Contracts extends FetchElement {
  static get styles() {
    return css([style]);
  }

  connectedCallback() {
    // TODO(piotrek): fetch real API
    const urls = { contracts: APP_HOST + 'api/contracts.json' };
    super.connectedCallback(urls);
  }

  renderBeforeFetch() {
    return html`
      <h2>Wartungen</h2>
      Loading...
    `
  }

  renderAfterFetch(data) {
    let processedJson = data.contracts.map(contract => {
      return {
        ...contract,
        creationTime: format(new Date(contract.creationTime), 'd.MM.YYY h:mm', { locale: deLocale })
      }
    });

    return html`
      <h2>Wartungen</h2>
      <vaadin-grid .items=${processedJson} @active-item-changed=${this.onActiveItemChanged}>
        <vaadin-grid-filter-column flex-grow="0" path="id" header="#" width="160px"></vaadin-grid-filter-column>
        <vaadin-grid-filter-column resizable flex-grow="1" path="name" header="Wartung"></vaadin-grid-filter-column>
        <vaadin-grid-filter-column resizable flex-grow="1" path="company.name" header="Firma"></vaadin-grid-filter-column>
        <vaadin-grid-column flex-grow="0" width="170px" path="creationTime" header="Erstellt"></vaadin-grid-column>
        <vaadin-grid-column flex-grow="0" width="80px" header="Erledigt" .renderer=${(root, col, rowData) =>
          render(rowData.item.done ? html`<i class="material-icons">done</i>` : '', root)}></vaadin-grid-column>
      </vaadin-grid>
    `
  }

  onActiveItemChanged(event) {
    if (event.detail.value) {
      router.constructor.go(URL_CONTRACTS + '/' + event.detail.value.id)
    }
  }
}
customElements.define('x-current-contracts', Contracts);
