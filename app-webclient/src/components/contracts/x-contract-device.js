import { FetchElement } from '../fetch-element';
import { html, css } from 'lit-element';
import style from '../common.scss'

import { format } from 'date-fns/esm';
import { de as deLocale } from 'date-fns/esm/locale'

import '@vaadin/vaadin-grid/vaadin-grid.js';
import { detailsRenderer, requiredRenderer, doneRenderer } from './x-contract-device-helpers';

class ContractDevice extends FetchElement {
  static get styles() {
    return [
      css([style]),
      css`
      vaadin-grid {
        height: calc(100vh - 272px);
      }`
    ];
  }

  connectedCallback() {
    // TODO(piotrek): fetch real API
    const urls = { contractDevice: APP_HOST + 'api/contract-device.json' };
    super.connectedCallback(urls);
  }

  renderAfterFetch(data) {
    return html`
      <h2>Wartung #${this.location.params.id} - ${data.contractDevice.name}</h2>
      <p><b>Ort: </b>
        ${data.contractDevice.address}</p>
      <p><b>Mitarbeiter vor Ort: </b>
        <a href='/accounts/${data.contractDevice.employee.id}'>${data.contractDevice.employee.name}</a></p>
      <p><b>Geplanter Termin: </b>
        ${data.contractDevice.appointmentTime? 
          html`${format(new Date(data.contractDevice.appointmentTime), 'd MMMM YYY hh:mm', { locale: deLocale })}</p>` : 
          html`Noch nicht verabredet`}
      </p>

      <vaadin-grid .items=${data.contractDevice.steps} .rowDetailsRenderer=${detailsRenderer} @active-item-changed=${this.onActiveItemChanged}>
        <vaadin-grid-column flex-grow="1" header="Schritt" path="title"></vaadin-grid-column>
        <vaadin-grid-column flex-grow="0" header="Erforderlich" width="120px" .renderer=${requiredRenderer}></vaadin-grid-column>
        <vaadin-grid-column flex-grow="0" header="Erledigt" width="100px" .renderer=${doneRenderer}></vaadin-grid-column>
      </vaadin-grid>
    `
  }

  onActiveItemChanged(event) {
    const item = event.detail.value;
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    grid.detailsOpenedItems = (grid.detailsOpenedItems[0] == item) ? [] : [item]
    grid.notifyResize();
  }

}

customElements.define('x-contract-device', ContractDevice);
