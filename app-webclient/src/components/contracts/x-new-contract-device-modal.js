import { FetchElement } from '../fetch-element.js';
import { html } from 'lit-element';
import { FETCH_ERROR } from '../templates.js';

import '../../app-layout/x-ok-dialog.js';
import './x-new-contract-device-modal-form.js'

class NewContractDeviceModal extends FetchElement {
  connectedCallback() {
    // console.log('device-modal connectedCallback');
    
    let urlsToFetch = {
      device: APP_HOST + 'api/devices/' + this.deviceId + '.json',
    };
    super.connectedCallback(urlsToFetch);
  }

  renderBeforeFetch() {
    return html`
      <x-ok-dialog title='Bitte warten' .template=${html`Die Anlage wird heruntergeladen...`} .okText=${"Abbrechen"} .onOk=${() => this.remove()}></x-ok-dialog>
    `;
  }

  renderAfterFetch() {
    return html`
      <x-new-contract-device-modal-form .deviceData=${this.fetchedData.device} .employees=${this.employees} .onYes=${this.onYes}></x-new-contract-device-modal-form>
    `;
  }

  renderOnFetchError(failedUrls) {
    return html`
      <x-ok-dialog title='Error' .template=${FETCH_ERROR(failedUrls)}></x-ok-dialog>
    `;
  }

}
customElements.define('x-new-contract-device-modal', NewContractDeviceModal);
