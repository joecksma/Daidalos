import { format, parse } from 'date-fns/esm';
import { de as deLocale } from 'date-fns/esm/locale'

export const datePickerI18n = {
  // An array with the full names of months starting
  // with January.
  monthNames: [
    'Januar', 'Februar', 'März', 'April', 'Mai',
    'Juni', 'Juli', 'August', 'September',
    'Oktober', 'November', 'Dezember'
  ],

  // An array of weekday names starting with Sunday. Used
  // in screen reader announcements.
  weekdays: [
    'Sonntag', 'Montag', 'Dienstag', 'Mittwoch',
    'Donerstag', 'Freitag', 'Samstag'
  ],

  // An array of short weekday names starting with Sunday.
  // Displayed in the calendar.
  weekdaysShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],

  // An integer indicating the first day of the week
  // (0 = Sunday, 1 = Monday, etc.).
  firstDayOfWeek: 1,

  // Used in screen reader announcements along with week
  // numbers, if they are displayed.
  week: 'Woche',

  // Translation of the Calendar icon button title.
  calendar: 'Kalendar',

  // Translation of the Clear icon button title.
  clear: 'Löschen',

  // Translation of the Today shortcut button text.
  today: 'Heute',

  // Translation of the Cancel button text.
  cancel: 'Schließen',

  // A function to format given `Object` as date string. Object is in the format `{ day: ..., month: ..., year: ... }`
  formatDate: d => {
    // console.log('formatting date', d)

    // returns a string representation of the given
    // object in 'MM/DD/YYYY' -format
    return format(new Date(d.year, d.month, d.day), 'd MMMM yyyy', { locale: deLocale })
  },

  // A function to parse the given text to an `Object` in the format `{ day: ..., month: ..., year: ... }`.
  // Must properly parse (at least) text formatted by `formatDate`.
  // Setting the property to null will disable keyboard input feature.
  parseDate: datePickerText => {
    // Parses a string in 'MM/DD/YY', 'MM/DD' or 'DD' -format to
    // an `Object` in the format `{ day: ..., month: ..., year: ... }`.
    // console.log('parsing dateComboText', datePickerText)
    let parsedDate = parse(datePickerText, 'd MMMM yyyy', new Date(), { locale: deLocale })
    let dateForPicker = {
      day: parsedDate.getUTCDate()+1,
      month: parsedDate.getUTCMonth(),
      year: parsedDate.getUTCFullYear()
    }
    return dateForPicker;
  },

  // A function to format given `monthName` and
  // `fullYear` integer as calendar title string.
  formatTitle: (monthName, fullYear) => {
    return monthName + ' ' + fullYear;
  }
}