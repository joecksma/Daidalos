import { STEP_TYPES } from "./contracts/step-types";

export const postData = (url = ``, data = {}) => {

  // Default options are marked with *
  return fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, cors, *same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    headers: {
      "Content-Type": "application/json",
      // "Content-Type": "application/x-www-form-urlencoded",
    },
    redirect: "follow", // manual, *follow, error
    referrer: "no-referrer", // no-referrer, *client
    body: JSON.stringify(data), // body data type must match "Content-Type" header
  })
    .then(response => response.json()); // parses JSON response into native Javascript objects 
}

export const getCompaniesComboFormat = fetchedCompaniesData =>
  fetchedCompaniesData.map(company => {
    return {
      ...company,
      value: company.id,
      label: company.name
    }
  }).sort((a, b) => a.label.localeCompare(b.label));

export const getDevicesComboFormat = fetchedDevicesData =>
  fetchedDevicesData.map(device => {
    return {
      ...device,
      value: device.id,
      label: device.id + ', ' + device.name
    }
  });

export const getEmployeesComboFormat = fetchedEmployeesData =>
  fetchedEmployeesData.map(employee => {
    return {
      ...employee,
      value: employee.id,
      label: employee.name
    }
  }).sort((a, b) => a.label.localeCompare(b.label));

export const getDeviceWithFlattenedSteps = deviceData => {
  // console.log('deviceData:', deviceData)
  const flattenedSteps = deviceData.steps.flatMap(step => step.type == STEP_TYPES.CHECKLIST ? step.checklist : step);
  return {
    ...deviceData,
    steps: flattenedSteps
  }
}

export const formLayoutResponsiveSteps = [
  { minWidth: 0, columns: 1 },
  { minWidth: '40em', columns: 2 },
  { minWidth: '80em', columns: 3 }
]
