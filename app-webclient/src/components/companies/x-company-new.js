import { html, css, LitElement } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';

import '../../app-layout/mdc-dialog.js'
import { URL_COMPANIES } from '../../app-layout/menu-contents';
// import { URL_EMPLOYEES } from '../../app-layout/menu-contents';

class NewCompany extends LitElement {
  static get styles() {
    return css([style]);
  }

  // promptChanges() {
  //   const company = {};
  //   company.id = this.location.params.id;
  //   company.name = this.renderRoot.getElementById('nameField').value;
  //   company.email = this.renderRoot.getElementById('emailField').value;
  //   company.phone = this.renderRoot.getElementById('phoneField').value;

  //   const dialog = document.createElement('x-dialog');
  //   dialog.title = 'Speichern';
  //   dialog.template = html`Änderungen speichern?`;
  //   dialog.onYes = () => {
  //     // TODO(piotrek): HTTP POST request
  //     console.warn('TODO(piotrek): updating company entry', company);
  //   }

  //   this.renderRoot.appendChild(dialog);
  // }

  render() {
    return html`
    <button @click=${this.promptChanges} class="mdc-button mdc-button--raised" style='float:right;'>
      <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
      <span class="mdc-button__label">Speichern</span>
    </button>
    <h2>Auftragnehmer - neuer Eintrag</h2>
    <vaadin-form-layout>
      <vaadin-text-field id='nameField' label="Name"></vaadin-text-field>
      <vaadin-text-field id='emailField' label="Email"></vaadin-text-field>
      <vaadin-text-field id='phoneField' label="Telefon"></vaadin-text-field>
    </vaadin-form-layout>
    
    <p></p>
  `
  }

}

customElements.define('x-company-new', NewCompany);
