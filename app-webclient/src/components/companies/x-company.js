import { FetchElement } from '../fetch-element';
import { html, css } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';

import '../../app-layout/mdc-dialog.js'
import { URL_COMPANIES } from '../../app-layout/menu-contents';

class Company extends FetchElement {
  static get styles() {
    return css([style]);
  }

  connectedCallback() {
    // TODO(piotrek): fetch real API
    const companyId = this.location.params.id;
    const urls = { company: APP_HOST + 'api/companies/'+ companyId +'.json' };
    super.connectedCallback(urls);
  }

  promptDelete() {
    const dialog = document.createElement('x-dialog');
    dialog.title = 'Löschen';
    dialog.template = html`Sind Sie sicher?`;
    dialog.onYes = () => {
      // TODO(piotrek): HTTP DELETE request
      console.warn('TODO(piotrek): deleting company entry', this.location.params.id);
      
      // navigate back after deletion
      location.href = URL_COMPANIES;
    }

    this.renderRoot.appendChild(dialog);
  }

  promptUpdate() {
    const company = {};
    company.id = this.location.params.id;
    company.name = this.renderRoot.getElementById('nameField').value;
    company.email = this.renderRoot.getElementById('emailField').value;
    company.phone = this.renderRoot.getElementById('phoneField').value;

    const dialog = document.createElement('x-dialog');
    dialog.title = 'Speichern';
    dialog.template = html`Änderungen speichern?`;
    dialog.onYes = () => {
      // TODO(piotrek): HTTP POST request
      console.warn('TODO(piotrek): updating company entry', company);
    }

    this.renderRoot.appendChild(dialog);
  }

  renderBeforeFetch() {
    return html`
      <h2>Auftragnehmer #${this.location.params.id}</h2>
      Loading...
    `
  }

  renderAfterFetch(data) {
    return html`
    <button @click=${this.promptDelete} class="mdc-button mdc-button--outlined" style="float:right; border-color:red; color:red; margin-left:16px;">
      <i class="material-icons mdc-button__icon" aria-hidden="true">delete</i>
      <span class="mdc-button__label">Löschen</span>
    </button>
    <button @click=${this.promptUpdate} class="mdc-button mdc-button--raised" style='float:right;'>
      <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
      <span class="mdc-button__label">Speichern</span>
    </button>
    <h2>Auftragnehmer #${this.location.params.id}</h2>
    <vaadin-form-layout>
      <vaadin-text-field id='nameField' label="Name" value="${data.company.name}"></vaadin-text-field>
      <vaadin-text-field id='emailField' label="Email" value="${data.company.email}"></vaadin-text-field>
      <vaadin-text-field id='phoneField' label="Telefon" value="${data.company.phone}"></vaadin-text-field>
    </vaadin-form-layout>
    
    <p></p>
  `
  }

}

customElements.define('x-company', Company);
