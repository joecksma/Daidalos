import { html } from "lit-html";

// Returns TemplateResult with info about failed fetch call.
// May be used as part of returned catch template.
export const FETCH_ERROR = urls => html`
  <p>Error while fetching data. Failed urls: <ul>
    ${urls.map(url => html`<li><a href=${url}>${url}</a></li>`)}
  </ul>
`;

export const GO_BACK = () => html`
  <p><a href='#' @click=${() => history.back()}>Go back</a></p>
`;
