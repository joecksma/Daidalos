import { LitElement, html } from 'lit-element';

class Page404 extends LitElement {
  render() {
    return html`
      <h2>404 :(</h2>
      <p>The page does not exist or may be not yet implemented. Check your link or try choosing one of menu links.</p>
      <p><a href='#' @click=${() => history.back()}>Go back</a></p>
    `;
  }

}
customElements.define('x-404', Page404);
