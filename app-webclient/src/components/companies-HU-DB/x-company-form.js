import { html, LitElement } from 'lit-element';

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';

class CompanyForm extends LitElement {
  static get properties() {
    return {
      company: { type: Object }
    }
  }

  updated(changedProperties) {
    // console.log(changedProperties);
    this.dispatchEvent(new CustomEvent('company-change', {
      detail: {
        company: this.company
      }
    }));
  }

  firstUpdated() {
    if (this.company.name) this.renderRoot.getElementById('name').value = this.company.name;
    if (this.company.email) this.renderRoot.getElementById('email').value = this.company.email;
    if (this.company.phone) this.renderRoot.getElementById('phone').value = this.company.phone;
  }

  onNameInput(event) {
    this.company = {
      ...this.company,
      name: event.composedPath()[0].value
    }
  }

  onEmailInput(event) {
    this.company = {
      ...this.company,
      email: event.composedPath()[0].value
    }
  }


  onPhoneInput(event) {
    this.company = {
      ...this.company,
      phone: event.composedPath()[0].value
    }
  }

  render() {
    return html`
      <vaadin-form-layout>
        <vaadin-text-field id='name' label="Name" @input=${this.onNameInput}></vaadin-text-field>
        <vaadin-text-field id='email' label="Email" @input=${this.onEmailInput}></vaadin-text-field>
        <vaadin-text-field id='phone' label="Telefon" @input=${this.onPhoneInput}></vaadin-text-field>
      </vaadin-form-layout>
    `
  }

}

customElements.define('x-company-form', CompanyForm);
