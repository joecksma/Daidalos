import { FetchElement } from '../fetch-element';
import { html, css } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-filter-column';
import { URL_EMPLOYEES } from '../../app-layout/menu-contents';

let router = '';
export const setRouter = r => router = r;

class Employees extends FetchElement {
  static get styles() {
    return css([style]);
  }

  connectedCallback() {
    const urls = { employees: HU_SERVER_URL + 'api/employee/read.php' };
    super.connectedCallback(urls);
  }

  renderBeforeFetch() {
    return html`
      <h2>Mitarbeiter</h2>
      Loading...
    `
  }

  renderAfterFetch(data) {
    return html`
      <a href=${URL_EMPLOYEES + '/new'} class="mdc-button mdc-button--raised" style='float:right;'>
        <i class="material-icons mdc-button__icon" aria-hidden="true">add</i>
        <span class="mdc-button__label">Hinzufügen</span>
      </a>
      <h2>Mitarbeiter</h2>
      <vaadin-grid .items=${data.employees} @active-item-changed=${this.onActiveItemChanged}>
        <vaadin-grid-filter-column flex-grow="0" path="id" header="#" width="160px"></vaadin-grid-filter-column>
        <vaadin-grid-filter-column flex-grow="1" path="name" header="Name"></vaadin-grid-filter-column>
        <vaadin-grid-filter-column flex-grow="1" path="email" header="E-Mail"></vaadin-grid-filter-column>
      </vaadin-grid>
    `
  }

  onActiveItemChanged(event) {
    if (event.detail.value) {
      router.constructor.go(URL_EMPLOYEES + '/' + event.detail.value.id)
    }
  }

}
customElements.define('x-employees', Employees);
