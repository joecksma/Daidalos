import { FetchElement } from '../fetch-element';
import { html, css } from 'lit-element';
import style from '../common.scss'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';

import '../../app-layout/mdc-dialog.js'
import { URL_COMPANIES } from '../../app-layout/menu-contents';
import { postData } from '../helpers.js';
import './x-company-form.js'

let router = '';
export const setRouter = r => router = r;

class Company extends FetchElement {
  static get styles() {
    return css([style])
  }

  static get properties() {
    return {
      // device: { type: Object },
      formComplete: { type: Boolean }
    }
  }

  connectedCallback() {
    // TODO(piotrek): fetch real API
    const urlsToFetch = {};
    const companyId = this.location.params.id;
    if (companyId) urlsToFetch.company = HU_SERVER_URL + 'api/company/read_one.php?id=' + companyId;
    super.connectedCallback(urlsToFetch);
  }

  promptDelete() {
    const company = { id: this.location.params.id };

    const dialog = document.createElement('x-dialog');
    dialog.title = 'Löschen';
    dialog.template = html`Sind Sie sicher?`;
    dialog.onYes = () => {
      const DELETE_URL = HU_SERVER_URL + 'api/company/delete.php';
      postData(DELETE_URL, company).then(() => router.constructor.go(URL_COMPANIES));
    }

    this.renderRoot.appendChild(dialog);
  }

  promptUpdate() {
    const dialog = document.createElement('x-dialog');
    dialog.title = 'Speichern';
    dialog.template = html`Änderungen speichern?`;
    dialog.onYes = () => {
      const UPDATE_URL = HU_SERVER_URL + 'api/company/update.php';
      postData(UPDATE_URL, this.company).then(() => router.constructor.go(URL_COMPANIES));
    }

    this.renderRoot.appendChild(dialog);
  }

  promptSave() {
    const dialog = document.createElement('x-dialog');
    dialog.title = 'Speichern';
    dialog.template = html`Neuen Eintrag speichern?`;
    dialog.onYes = () => {
      // console.log(company)
      const CREATE_URL = HU_SERVER_URL + 'api/company/create.php';
      postData(CREATE_URL, this.company).then(() => router.constructor.go(URL_COMPANIES));
    }

    this.renderRoot.appendChild(dialog);
  }

  renderBeforeFetch() {
    return html`
      <h2>Auftragnehmer</h2>
      Loading...
    `
  }

  renderAfterFetch() {
    if (!this.company) this.company = this.fetchedData.company ? this.fetchedData.company : {};

    if (this.fetchedData.company) {
      // editing company
      return html`
        <button @click=${this.promptDelete} class="mdc-button mdc-button--outlined" style="float:right; border-color:red; color:red; margin-left:16px;">
          <i class="material-icons mdc-button__icon" aria-hidden="true">delete</i>
          <span class="mdc-button__label">Löschen</span>
        </button>
        <button ?disabled=${!this.formComplete} @click=${this.promptUpdate} class="mdc-button mdc-button--raised" style='float:right;'>
          <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
          <span class="mdc-button__label">Speichern</span>
        </button>
        <h2>Auftragnehmer #${this.location.params.id}</h2>
        <x-company-form .company=${this.company} @company-change=${this.onCompanyChange}></x-company-form>
      `
    } else {
      // new company
      return html`
        <button ?disabled=${!this.formComplete} @click=${this.promptSave} class="mdc-button mdc-button--raised" style='float:right;'>
          <i class="material-icons mdc-button__icon" aria-hidden="true">save</i>
          <span class="mdc-button__label">Speichern</span>
        </button>
        <h2>Neuer Auftragnehmer</h2>
        <x-company-form .company=${this.company} @company-change=${this.onCompanyChange}></x-company-form>
      `
    }
  }

  onCompanyChange(event) {
    // console.log(event)
    this.company = event.detail.company; //property is not observed
    this.formComplete = Boolean(this.company.name && this.company.email && this.company.phone); //observed property, change causes rerender
  }

}

customElements.define('x-company', Company);
