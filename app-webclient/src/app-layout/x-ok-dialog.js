import { DialogElement } from './mdc-dialog';
import { html } from 'lit-element';

export class OKDialogElement extends DialogElement {

  render() {
    return html`
      <div class="mdc-dialog mdc-dialog--open mdc-dialog--scrollable" role="dialog" aria-modal="true" aria-labelledby="mdc-dialog-with-list-label" aria-describedby="mdc-dialog-with-list-description">
        <div class="mdc-dialog__scrim"></div>
        <div class="mdc-dialog__container">
          <div class="mdc-dialog__surface" style=${this.surfaceStyle ? `${this.surfaceStyle}` : ``}>
            <h2 id="mdc-dialog-with-list-label" class="mdc-dialog__title">${this.title}</h2>
            <section id="mdc-dialog-with-list-description" class="mdc-dialog__content">
              ${this.template}
            </section>
            <footer class="mdc-dialog__actions">
              <button @click=${this.onOk} class="mdc-button mdc-button--raised" style='margin-left: 16px' data-mdc-dialog-action="ok">${this.okText || 'OK'}</button>
            </footer>
          </div>
        </div>
      </div>

    `;
  }

}
customElements.define('x-ok-dialog', OKDialogElement);
