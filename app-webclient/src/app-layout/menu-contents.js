export const URL_CONTRACTS = '/contracts';
export const URL_NEW_CONTRACT = '/contracts/new';
export const URL_DUPLICATE_CONTRACT = '/contracts/duplicate';
export const URL_DEVICES = '/devices';
export const URL_COMPANIES = '/companies';
export const URL_EMPLOYEES = '/employees';

export const MENU_CONTENTS = [
  [
    {
      label: 'Wartungen',
      icon: 'folder_open',
      url: URL_CONTRACTS
    }, {
      label: 'Neue Wartung',
      icon: 'create_new_folder',
      url: URL_NEW_CONTRACT
    },
    {
      label: 'Anlagen',
      icon: 'dashboard',
      url: URL_DEVICES
    }
  ],
  [
    {
      label: 'Auftragnehmer',
      icon: 'business',
      url: URL_COMPANIES
    },
    {
      label: 'Mitarbeiter',
      icon: 'people',
      url: URL_EMPLOYEES
    }
  ],
  [
    {
      label: 'Logout',
      icon: 'exit_to_app',
      url: '#'
    }
  ],
];
