import { LitElement, html, css } from 'lit-element';
import { MDCDialog } from '@material/dialog';

import style from './mdc-dialog.scss';

export class DialogElement extends LitElement {

  static get styles() {
    return css([style])
  }

  render() {
    return html`
      <div class="mdc-dialog mdc-dialog--open mdc-dialog--scrollable" role="dialog" aria-modal="true" aria-labelledby="mdc-dialog-with-list-label" aria-describedby="mdc-dialog-with-list-description">
        <div class="mdc-dialog__scrim"></div>
        <div class="mdc-dialog__container">
          <div class="mdc-dialog__surface" style=${this.surfaceStyle ? `${this.surfaceStyle}` : ``}>
            <h2 id="mdc-dialog-with-list-label" class="mdc-dialog__title">${this.title}</h2>
            <section id="mdc-dialog-with-list-description" class="mdc-dialog__content">
              ${this.template}
            </section>
            <footer class="mdc-dialog__actions">
              <button @click=${this.onCancel} class="mdc-button" data-mdc-dialog-action="cancel">${this.cancelText || 'Abbrechen'}</button>
              <button @click=${this.onYes} class="mdc-button mdc-button--raised" style='margin-left: 16px' data-mdc-dialog-action="yes">${this.yesText || 'Ja'}</button>
            </footer>
          </div>
        </div>
      </div>
    `;
  }

  firstUpdated() {
    const dialog = new MDCDialog(this.renderRoot.querySelector('.mdc-dialog'));
    dialog.scrimClickAction = ''; //prevents closing dialog after click on scrim (background)
    dialog.open();
  }

}
customElements.define('x-dialog', DialogElement);
