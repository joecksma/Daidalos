import { LitElement, html, css } from 'lit-element';
// TODO: make drawer dismissible
// import { MDCTopAppBar } from "@material/top-app-bar";
// import { MDCList } from "@material/list";

import { MDCDrawer } from '@material/drawer'

import style from './app-layout.scss'
import { MENU_CONTENTS } from './menu-contents';

// Changes the (default light blue) color of Vaadin Grid label.
const VAADIN_THEME_COLOR_FIX = html`
  <dom-module id="aa" theme-for="vaadin-text-field vaadin-text-area vaadin-upload">
    <template>
      <style>
        :host {
          --lumo-primary-text-color:#00376c;
        }
      </style>
    </template>
  </dom-module>

    <dom-module id="b" theme-for="vaadin-upload">
      <template is="dom-repeat" items="{{files}}" as="file">
        <li>
          <strong>[[file.name]]</strong>
          [[file.status]]
        </li>
      </template>
  </dom-module>

  <dom-module id="grid-styles" theme-for="vaadin-grid">
    <template>
        <style>
          :host [part~="row"]:hover [part~="body-cell"]{
            /* background-color: rgba(0, 55, 108, 0.12); */
            background-color: rgba(0, 0, 0, 0.02);
          } 

          :host [part~="body-cell"] ::slotted(vaadin-grid-cell-content){
            cursor: pointer;
          }
        </style>
    </template>
  </dom-module> 
`;

const APP_VERSION = PACKAGE_VERSION + ' : ' + new Date(BUILD_TIMESTAMP).toLocaleDateString();

class AppLayout extends LitElement {
  static get styles() {
    return css([style]);
  }

  static get properties() {
    return {
      selectedMenuItemUrl: { type: String },
    };
  }

  toogleDrawer() {
    const drawer = new MDCDrawer.attachTo(this.renderRoot.querySelector('.mdc-drawer'));
    drawer.open = !drawer.open;
  }

  render() {
    return html`
      ${VAADIN_THEME_COLOR_FIX}
      
      <aside class="mdc-drawer mdc-drawer--dismissible mdc-drawer--open">
        <div class="mdc-drawer__header">
          <div class="mdc-drawer__header-content" style="padding-top: 24px">
            <img src="/assets/hu192.png" height="48dp">
            <h3 class="mdc-drawer__title">Max Mustermann</h3>
            <h6 class="mdc-drawer__subtitle">HU Berlin</h6>
          </div>
        </div>
      
        <div class="mdc-drawer__content">
          <div class="mdc-list">
            ${MENU_CONTENTS.map(menuGroup => html`
            ${menuGroup.map(menuItem => html`
            <a class='${this.selectedMenuItemUrl == menuItem.url ? ' mdc-list-item--activated' : '' } mdc-list-item' href=${menuItem.url}>
              <i class="material-icons mdc-list-item__graphic" aria-hidden="true">${menuItem.icon}</i>
              <span class="mdc-list-item__text">${menuItem.label}</span>
            </a>`)}
            <hr class="mdc-list-divider">`)}
            <p class='mdc-list-item' style="text-align: center;">App version: ${APP_VERSION}</p>
          </div>
        </div>
      </aside>
      
      <div class="mdc-drawer-app-content">
        <header class="mdc-top-app-bar app-bar" id="app-bar">
          <div class="mdc-top-app-bar__row">
            <section class="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
              <a href="#" class="demo-menu material-icons mdc-top-app-bar__navigation-icon" @click=${this.toogleDrawer}>menu</a>
              <span class="mdc-top-app-bar__title">Daidalos</span>
            </section>
          </div>
        </header>
      
        <main class="main-content" id="main-content">
          <slot name="pages"></slot>
        </main>
      </div>
    `;
  }
}

customElements.define('app-layout', AppLayout);
