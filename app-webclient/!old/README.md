## Demo
https://daidalos-backend.firebaseapp.com/

## Installation
Clone the repo and run
```bash
npm install
```
to download all node dependencies.

## Development
```bash
npm start
```
runs [webpack-web-server](https://webpack.js.org/configuration/dev-server/).

## Build
```bash
npm run build
```
builds the minified bundle and uploads the newest version to firebase.