export const URL_CURRENT_JOBS = 'current-contracts'; 
export const URL_NEW_JOB = 'new-contract';
export const URL_FINISHED_JOBS = 'closed-contracts';
export const URL_RESOURCES = 'resources';

export const URL_COMPANIES = 'companies';
export const URL_ACCOUNTS = 'accounts';
export const URL_SETTINGS = 'settings';

export const MENU_CONTENTS = [
    [
        {
            label: 'Aktuelle Wartungen',
            icon: 'folder_open',
            url: URL_CURRENT_JOBS
        }, {
            label: 'Neue Wartung',
            icon: 'create_new_folder',
            url: URL_NEW_JOB
        }, {
            label: 'Abfertigte Wartungen',
            icon: 'folder',
            url: URL_FINISHED_JOBS
        }, {
            label: 'Anlagen',
            icon: 'dashboard',
            url: URL_RESOURCES
        }
    ],
    [
        {
            label: 'Auftragnehmer',
            icon: 'business',
            url: URL_COMPANIES
        }
    ],
    [
        {
            label: 'Kontenverwaltung',
            icon: 'people',
            url: URL_ACCOUNTS
        }
    ],
    [
        {
            label: 'Einstellungen',
            icon: 'settings',
            url: URL_SETTINGS
        },
        {
            label: 'Logout',
            icon: 'exit_to_app',
            url: '#'
        }
    ],
];
