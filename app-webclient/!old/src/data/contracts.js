let DUMMY_CONTRACT = {
    name: "Aufzüge im ESZ",
    companyName: "ABC Aufzüge GmbH",
    startDate: "2019-01-10",
    endDate: "2019-02-28",
    token:"125678902345678",
    remarks: ""
}

export const CONTRACTS = new Array(5).fill().map((e, i) => {

    return {
        ...DUMMY_CONTRACT, id: i
    }
});
