
import './_common.scss';
import './current-jobs.scss'

import { LitElement, html } from 'lit-element';

import { MDCTopAppBar } from "@material/top-app-bar";
import { MDCList } from "@material/list";

import { MENU_CONTENTS, URL_CURRENT_JOBS } from '../data/menu_contents';
import './dialog.js'
import { CONTRACTS } from '../data/contracts';

import tableDragger from 'table-dragger'


class CurrentJobs extends LitElement {

    createRenderRoot() { return this }

    static get properties() {
        return {
            page: { type: String },
        }
    }

    constructor() {
        super();
        this.page = URL_CURRENT_JOBS;
        console.log(CONTRACTS)
    }

    render() {

        return html`
  <style>
    table {
      table-layout: auto;
      width: 100%;
      border: 1px solid black;
      background-color: #00376c11;

    }

    .sindu_dragger table{
        background: none;
    }

    /* .sindu_row {
        background-color: purple;
    } */

    .sindu-table {
      /* color: #fff; */
      /* overflow: hidden; */
    }
    .sindu-table th {
      padding: 16px;
      /* color: #fff; */
    }
    .sindu-table td {
      padding: 16px;
        }

        /* .gu-transit { 
  color: green;
} */
  </style>


        <h2>Aktuelle Wartungen</h2>
        <table>
            <thead>
                <tr style="text-align:left; padding:2px">
                    <th style="width:30px"></th>
                    <th>ID</th>
                    <th>Auftrag</th>
                    <th>Firma</th>
                    <th>Anfangsdatum</th>
                    <th>Enddatum</th>
                    <th>Kommentar</th>
                </tr>
            </thead>

            <tbody>
                ${CONTRACTS.map(contract => html`
                <tr>
                    <td><i class="material-icons">unfold_more</i></td>
                    <td>${contract.id}</td>
                    <td>${contract.name}</td>
                    <td>${contract.companyName}</td>
                    <td>${contract.token}</td>
                    <td>${contract.startDate}</td>
                    <td>${contract.endDate}</td>
                    <td>${contract.remarks}</td>
                </tr>
                `)}
            </tbody>

        </table>

    `;
    }

    _showDialog(e) {
        let dialog = document.createElement('x-dialog');
        dialog.title = e.currentTarget.dataset['title'];
        this.appendChild(dialog);
    }

    firstUpdated() {
        const topAppBarElement = document.querySelector('.mdc-top-app-bar');
        const topAppBar = new MDCTopAppBar(topAppBarElement);

        const list = MDCList.attachTo(document.querySelector('.mdc-list'));
        list.wrapFocus = true;

        let td = tableDragger(document.querySelector('table'), {
            mode: 'row',
            onlyBody: true,
            animation: 300
        });

        console.log('td', td)
    }
}

customElements.define('current-jobs', CurrentJobs);
