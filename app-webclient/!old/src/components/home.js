console.log('Hi from home.js');

import './_common.scss';
import './current-jobs.js';

import { LitElement, html } from 'lit-element';

import { MDCTopAppBar } from "@material/top-app-bar";
import { MDCList } from "@material/list";

import { MENU_CONTENTS, URL_CURRENT_JOBS } from '../data/menu_contents';
import './dialog.js'
import { CONTRACTS } from '../data/contracts';
import { Router } from '@vaadin/router';


import tableDragger from 'table-dragger'


class AppElement extends LitElement {

    createRenderRoot() { return this }

    static get properties() {
        return {
            page: { type: String },
        }
    }

    constructor() {
        super();
        this.page = URL_CURRENT_JOBS;
        console.log(CONTRACTS)
    }

    render() {
        console.log('Page:', this.page);

        return html`
  <style>
    table {
      table-layout: auto;
      width: 100%;
      border: 1px solid black;
      background-color: #00376c11;

    }

    .sindu_dragger table{
        background: none;
    }

    /* .sindu_row {
        background-color: purple;
    } */

    .sindu-table {
      /* color: #fff; */
      /* overflow: hidden; */
    }
    .sindu-table th {
      padding: 16px;
      /* color: #fff; */
    }
    .sindu-table td {
      padding: 16px;
        }

        /* .gu-transit {
  color: green;
} */
  </style>

<aside class="mdc-drawer">
    <div class="mdc-drawer__header">
        <div class="mdc-drawer__header-content" style="padding-top: 24px">
            <img src="./assets/hu192.png" height="48dp">
        </div>
        <h3 class="mdc-drawer__title">Max Mustermann</h3>
        <h6 class="mdc-drawer__subtitle">HU Berlin</h6>
    </div>

    <div class="mdc-drawer__content">
        <div class="mdc-list">

            ${MENU_CONTENTS.map(menuGroup => html`
            ${menuGroup.map(menuItem => html`
            <a class='${menuItem.url == this.page ? ' mdc-list-item--activated' : ''} mdc-list-item' href=${menuItem.url}
                @click="${this._navigate}">
                <i class="material-icons mdc-list-item__graphic" aria-hidden="true">${menuItem.icon}</i>
                <span class="mdc-list-item__text">${menuItem.label}</span>
            </a>
            `)}
            <hr class="mdc-list-divider">
            `)}
        </div>
    </div>
</aside>

<div class="mdc-drawer-app-content">
    <header class="mdc-top-app-bar app-bar" id="app-bar">
        <div class="mdc-top-app-bar__row">
            <section class="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
                <span class="mdc-top-app-bar__title">Daidalos</span>
            </section>
        </div>
    </header>

    <main class="main-content" id="main-content">
        <!-- <button class="mdc-button mdc-button--raised" @click="${this._showDialog}" data-title="Wizard!">Click me to
            show dialog</button> -->



    </main>

</div>
    `;
    }

    _showDialog(e) {
        let dialog = document.createElement('x-dialog');
        dialog.title = e.currentTarget.dataset['title'];
        this.appendChild(dialog);
    }

    firstUpdated() {
        const topAppBarElement = document.querySelector('.mdc-top-app-bar');
        const topAppBar = new MDCTopAppBar(topAppBarElement);

        const list = MDCList.attachTo(document.querySelector('.mdc-list'));
        list.wrapFocus = true;

        const host = document.querySelector('main');
        const router = new Router(host);

        console.log(router)

        router.setRoutes([
            {path: '/', redirect: '/current-contracts'},

            {path: '/current-contracts',     component: 'current-jobs'},
            {path: '/users',  component: 'x-user-list'},
            {path: '/users/:user', component: 'x-user-profile'},
            {path: '(.*)', component: 'x-not-found-view'},
          ]);


    }
}

customElements.define('daidalos-app', AppElement);
