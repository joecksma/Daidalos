const path = require('path');
const webpack = require('webpack');
const cleanPlugin = require('clean-webpack-plugin');
const copyPlugin = require('copy-webpack-plugin');
const htmlPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = function (_, env) {
  const isProd = env.mode === 'production';

  return {
    mode: isProd ? 'production' : 'development',
    entry: {
      'index': './src/components/index.js',
      'home': './src/components/home.js',
    },
    devtool: isProd ? 'source-map' : 'inline-source-map',
    output: {
      filename: '[name].bundle.js',
    },

    devServer: {
      open: true,
      historyApiFallback: true, //enables rewrite to index.html
    },

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            isProd ? MiniCssExtractPlugin.loader : 'style-loader',
            'css-loader',
            {
              loader: 'sass-loader',
              options: {
                includePaths: ['./node_modules']
              }
            }
          ]
        },
      ]
    },

    plugins: [
      isProd && new cleanPlugin('./dist/*'),

      new copyPlugin([{ from: 'src/static', to: 'assets' }]),

      isProd && new webpack.optimize.SplitChunksPlugin({}),

      isProd && new MiniCssExtractPlugin({
        filename: '[name].[contenthash:5].css',
        chunkFilename: '[name].[contenthash:5].css'
      }),

      new htmlPlugin({
        filename: 'index.html',
        template: 'src/home.html',
        chunks: ['home'],
      }),
      new htmlPlugin({
        filename: 'home.html',
        template: 'src/home.html',
        chunks: ['home'],
      })
    ].filter(Boolean), // Filter out any falsey plugin array entries.

  }
}
