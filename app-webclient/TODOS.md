## Recommended
1. Handling of empty contract's appointment data (not yet scheduled, no account assigned).
2. Using old contracts as templates 

## Optional

1. Move all css to one file to avoid duplication of MDC styles in many page-components

2. Move all API endpoints to single 'configuration' file

3. ComboBox binding are done with late evaluation of name and looking for object id back in the original api response, because Vaadin ComboBox accepts only flat arrays. Maybe look for an alternative/ write own ComboBox that accepts {id:"", value:""} type