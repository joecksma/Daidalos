# API
> Current examples: https://daidalos-backend.firebaseapp.com/api/

## Contracts
- ```GET /contracts.php``` like https://daidalos-backend.firebaseapp.com/api/contracts.json
- ```GET /contract.php?id=1``` like https://daidalos-backend.firebaseapp.com/api/contract.json

REST-style: see https://tools.ietf.org/html/rfc7231#section-4.3
- ```PUT /contract.php```
- ```POST /contract.php?id=1``` 
- ```DELETE /contract.php?id=1```

or PHP-style:  
when it would be really hard to implement methods other than GET  
- ```GET /contract/create.php```  
- ```GET /contract/update.php?id=1```  
- ```GET /contract/delete.php?id=1```

## Devices
All devices, not only contract devices
- ```GET /devices.php``` like https://daidalos-backend.firebaseapp.com/api/devices.json
- ```GET /device.php?id=1``` like https://daidalos-backend.firebaseapp.com/api/device.json
- ```PUT /device.php```
- ```POST /device.php?id=1``` 
- ```DELETE /device.php?id=1```

## Companies
- ```GET /companies.php``` like https://daidalos-backend.firebaseapp.com/api/companies.json
- ```GET /company.php?id=1``` like https://daidalos-backend.firebaseapp.com/api/company.json
- ```PUT /company.php```
- ```POST /company.php?id=1``` 
- ```DELETE /company.php?id=1```

## Employees 
The first two are nice to have, but they are subject of the later connection with HU identity management system, so they can be faked, even with static responses. But probably when after implementing other endpoints these can be implemented in the blink of an eye. 
- ```GET /employees.php``` like https://daidalos-backend.firebaseapp.com/api/employees.json
- ```GET /employee.php?id=1``` like https://daidalos-backend.firebaseapp.com/api/employee.json

The latter will be probably not managed by us anyway
- ```PUT /employee.php```
- ```POST /employee.php?id=1``` 
- ```DELETE /employee.php?id=1```
