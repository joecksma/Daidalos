# Webtechnologien

- https://www.lynda.com/JavaScript-tutorials/JavaScript-Essential-Training/574716-2.html (Alle Lynda Kurse sind nach der Anmeldung mit dem HU-Konto verfügbar: https://www.lynda.com/signin/organization
)

- [lit-HTML (Chrome Dev Summit 2017)](https://www.youtube.com/watch?v=Io6JjgckHbg)

- [The Virtue of Laziness: Leveraging Incrementality for Faster Web UI (Chrome Dev Summit 2018)](https://www.youtube.com/watch?v=ypPRdtjGooc)