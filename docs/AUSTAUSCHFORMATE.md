# Neue Formate: https://daidalos-backend.firebaseapp.com/api/

## Alte:

Wartung

```json
{
  "steps": [
    {
      "type": "plan",
      "title": "Lageplan",
      "url": "http://asdfasdf.asd/asdf.pdf",
      "severity": "NONE",
      "done": true
    },
    {
      "type": "scanConfirm",
      "title": "Abgleich der Inventarnummer",
      "severity": "none",
      "done": true
    },
    {
      "type": "photos",
      "title": "Foto bei Ankunft",
      "results": [
        {
          "url": "asdfasdf",
          "timestamp": "asdf"
        },
        {
          "url": "asdfasdf",
          "timestamp": "asdf"
        }
      ],
      "severity": "REQUIRED",
      "done": true
    },
    {
      "type": "documents",
      "title": "Bereitgestellte Dokumente",
      "contents": [
        {
          "url": "asdfasdf",
          "title": "Letzte Wartung"
        },
        {
          "url": "asdfasdf",
          "title": "Letzte Mängelliste"
        }
      ],
      "severity": "none",
      "done": true
    },
    {
      "type": "action",
      "title": "Anlage herunterfahren",
      "feedback": {},
      "children": []
    },
    {
      "type": "action",
      "title": "Zentrales Feedback",
      "feedback": {},
      "severity": "none",
      "done": true
    },
    {
      "type": "photo",
      "title": "Fotos am Ende",
      "results": [
        {
          "url": "asdfasdf",
          "timestamp": "asdf"
        },
        {
          "url": "asdfasdf",
          "timestamp": "asdf"
        }
      ],
      "severity": "none",
      "done": true
    }
  ],
  "title": "Lüftungsanlage im Erdgeschoss",
  "deviceId": "35243345635624",
  "startTime": "2019-01-14T15:34:16+00:00",
  "endTime": "2019-01-14T15:34:16+00:00",
  "address": "Rudower Chaussee 25, 12489 Berlin",
  "done": false,
  "appointment": "???"
}
```

Ich würde keine userId da drin haben, sondern das komplett lokal verwalten

Die Fehlermeldung sollte sein: “Sie müssen den Schritt TITLE durchführen”
