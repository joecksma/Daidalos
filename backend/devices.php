<?php

include 'db.php';
include 'encode.php';

if(isset($_GET["auth"])){
	$token=$_GET["auth"];
}else{
	echo json_encode(["success"=>false,]);
	exit;
}

	
$stmt = $conn->prepare("SELECT device from auth where token = ?");
$stmt->bind_param("s", $token);
$stmt->execute();
$stmt_result = $stmt->get_result();

$contents=[];

while($row = $stmt_result->fetch_assoc()) {
	
	if(file_exists(encode($row["device"])."finished.json")){
		$contents[$row["device"]]=json_decode(file_get_contents(encode($row["device"])."finished.json"),true);
		
	}else{
	
		$contents[$row["device"]]=json_decode(file_get_contents(encode($row["device"])."data.json"),true);
	}
}
echo json_encode($contents);

?>