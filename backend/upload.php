<?php

include 'config.php';
include 'db.php';
include 'encode.php';
include 'random_str.php';

if (isset($_GET["auth"]) && isset($_GET["device"])) {
    $token = $_GET["auth"];
    $device = $_GET["device"];
} else {
    echo json_encode(["success" => false,]);
    exit;
}


$stmt = $conn->prepare("SELECT * from auth where token = ? and device = ?");
$stmt->bind_param("ss", $token, $device);
$stmt->execute();
$stmt_result = $stmt->get_result();

if ($stmt_result->num_rows > 0) {


    $image = $_POST['image'];
    $name = $_POST['name'];
    $realImage = base64_decode($image);


    $extention = pathinfo($name, PATHINFO_EXTENSION);
    $allowed = array('gif', 'png', 'jpg');
    if (!in_array($extention, $allowed)) {
        echo json_encode(["success" => false,]);
        exit;
    }

    $uploaddir = encode($device);
    $filename = random_str(15) . "." . $extention;
    $uploadfile = $uploaddir . $filename;


    file_put_contents($uploadfile, $realImage);
    echo json_encode(["success" => true, "url" => $web_address . "download.php?" . http_build_query(["device" => $device, "file" => $filename])]);


} else {

    echo json_encode(["success" => false,]);

}


?>