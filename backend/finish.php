<?php

include 'db.php';
include 'encode.php';

if (isset($_GET["auth"]) && isset($_GET["device"])) {
    $token = $_GET["auth"];
    $device = $_GET["device"];
} else {
    echo json_encode(["success" => false,]);
    exit;
}


$stmt = $conn->prepare("SELECT * from auth where token = ? and device = ?");
$stmt->bind_param("ss", $token, $device);
$stmt->execute();
$stmt_result = $stmt->get_result();

if ($stmt_result->num_rows > 0) {


    $uploaddir = encode($device);
    $filename = "finished.json";
    $uploadfile = $uploaddir . $filename;

    file_put_contents($uploadfile, $_POST["json"]);
    echo json_encode(["success" => true,]);
} else {

    echo json_encode(["success" => false,]);

}


?>