<?php

include 'db.php';
include 'encode.php';

if(isset($_GET["auth"])&&isset($_GET["device"])&&isset($_GET["file"])){
	$token=$_GET["auth"];
	$device=$_GET["device"];
	$file=basename($_GET["file"]);
}else{
	echo json_encode(["success"=>false,]);
	exit;
}


$stmt = $conn->prepare("SELECT * from auth where token = ? and device = ?");
$stmt->bind_param("ss", $token, $device);
$stmt->execute();
$stmt_result = $stmt->get_result();

if($stmt_result->num_rows>0){
	
	
	$filename = encode($device).$file;

	if(file_exists($filename)){

		//Get file type and set it as Content Type
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		header('Content-Type: ' . finfo_file($finfo, $filename));
		finfo_close($finfo);

		//Use Content-Disposition: attachment to specify the filename
		header('Content-Disposition: attachment; filename='.basename($filename));

		//No cache
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');

		//Define file size
		header('Content-Length: ' . filesize($filename));

		ob_clean();
		flush();
		readfile($filename);
		exit;
	}else{
	
		echo json_encode(["success"=>false,]);
	
	}
	
}else{
	
	echo json_encode(["success"=>false,]);
	
}




?>