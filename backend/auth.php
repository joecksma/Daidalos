<?php

include 'db.php';
include 'encode.php';

if(isset($_GET["auth"])){
	$token=$_GET["auth"];
}else{
	echo json_encode(["success"=>false,]);
	exit;
}


$stmt = $conn->prepare("SELECT device from auth where token = ?");
$stmt->bind_param("s", $token);
$stmt->execute();
$stmt_result = $stmt->get_result();

if($stmt_result->num_rows>0){
	
	echo json_encode(["success"=>true,]);
	
}else{
	
	echo json_encode(["success"=>false,]);
	
}

?>