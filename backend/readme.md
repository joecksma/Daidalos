### auth.php

Überprüft die Gültigkeit eines Tokens, das mittels ?auth=TOKEN übergeben wird, und gibt im Erfolgsfall {"success":true} aus.

```
http://141.20.37.114/auth.php?auth=HU
```

### devices.php

Gibt alle Informationen zu allen Geräten aus, die mit einem Token verknüpft wurden. Die Rückgabe erfolgt im json-Format.
Aufruf: In der URL muss das Token als ?auth=TOKEN angegeben werden.

```
http://141.20.37.114/devices.php?auth=HU
```

### download.php

Dient zum Herunterladen der zur Wartung gehörigen Bilddateien. Wird als URL beim Upload von Dateien benutzt und daher in die zum Gerät gehörenden Daten geschrieben. Wichtig ist, dass das Token als ?auth=TOKEN mitgeliefert wird. Außerdem müssen per GET device und file spezifiziert werden.

```
http://141.20.37.114/download.php?device=42142287&file=eEqgLX9TNn1ajYq.jpg&auth=HU
```

### finish.php

Dient zum Aktualisieren der Wartungs-Informationen zu einem bestimmten Gerät bei Abschluss der Wartung. Dazu müssen Gerät und Token spezifiziert werden: $token = $_GET["auth"] sowie $device = $_GET["device"].
$_POST["json"] muss die json-encodeten Informationen zum Gerät enthalten.                                                                                                                                   
                                                                                                                                   
### upload.php

Dient zum Hochladen von Bildern. Dazu müssen Gerät und Token spezifiziert werden: $token = $_GET["auth"] sowie $device = $_GET["device"].

$image = $_POST['image'] enthält das base64-encodete Bild.
    $name = $_POST['name'] enthält den Dateinamen.
    
Es wird ein JSON-String ausgegeben, der im Feld "url" den neuen Dateinamen enthält (via upload.php, beim Aufruf muss noch mittels &auth=TOKEN ein Token angefügt werden).