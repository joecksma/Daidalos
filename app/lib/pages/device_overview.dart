import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:scoped_model/scoped_model.dart';

import '../scoped-models/main.dart';
import '../models/device.dart';
import '../widgets/logoutDialog.dart';
import '../widgets/device_preview.dart';
import '../widgets/adaptive_progress_indicator.dart';
import './device.dart';
import './schedule.dart';
import './about.dart';

class DeviceOverviewPage extends StatefulWidget {
  final MainModel model;
  DeviceOverviewPage(this.model);

  DeviceOverviewPageState createState() => DeviceOverviewPageState();
}

class DeviceOverviewPageState extends State<DeviceOverviewPage> {
  String qrScanResult;

  Future<void> showError(String error) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(title: Text("Error"), content: Text(error));
      },
    );
  }

  Future scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        qrScanResult = qrResult;
        widget.model.selectDevice(qrScanResult);
        Device device = widget.model.selectedDevice;
        if (device == null) {
          showError('Gerät nicht gefunden!');
        } else {
          widget.model.setCodeScanned(true);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (BuildContext context) {
              return DevicePage();
            }),
          );
        }
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        showError("Kamera-Zugriff wurde verweigert!");
      } else {
        showError("Unknown Error $ex\nScan-Result: $qrScanResult");
      }
    } on FormatException {} on StateError catch (ex) {
      showError("Dieses Gerät ist nicht Teil Ihres Wartungsauftrags!\n$ex\nScan-Result: $qrScanResult");
    } catch (ex) {
      showError("Unknown Error $ex\nScan-Result: $qrScanResult");
    }
  }

  @override
  void initState() {
    super.initState();
    widget.model.fetchDevices(clearExisting: true);
    qrScanResult = "";
  }

  Widget _buildSignoutButton(BuildContext context) {
    return FlatButton(
      child: Text(
        "Abmelden",
        style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        LogoutDialog.onLogoutPressed(context);
      },
    );
  }

  Widget _buildAboutButton(BuildContext context) {
    return FlatButton(
      child: Icon(
        Icons.info_outline,
        color: Colors.white,
      ),
      onPressed: () {
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AboutPage()));
      },
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      actions: <Widget>[
        _buildAboutButton(context),
        _buildSignoutButton(context),
      ],
      bottom: TabBar(
        tabs: [
          Tab(
            child: Text(
              "Ausstehend",
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
          ),
          Tab(
            child: Text(
              "Erledigt",
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
          ),
        ],
        indicatorColor: Colors.white,
      ),
      title: Text('DAIDALOS'),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

  Widget _buildFloatingButton(BuildContext context, MainModel model) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          FloatingActionButton.extended(
              icon: Icon(Icons.schedule),
              label: Text("Termin"),
              backgroundColor: Theme.of(context).primaryColor,
              heroTag: UniqueKey(),
              onPressed: () {
                DateTime firstDate;
                DateTime lastDate;
                for (final device in model.unfinishedDevices) {
                  if (firstDate == null || lastDate == null) {
                    firstDate = device.startDate;
                    lastDate = device.endDate;
                  }
                  firstDate = (device.startDate.compareTo(firstDate) == -1) ? device.startDate : firstDate;
                  lastDate = (device.endDate.compareTo(lastDate) == 1) ? device.endDate : lastDate;
                }
                print(firstDate.compareTo(lastDate));
                if (firstDate == null || lastDate == null || firstDate.compareTo(lastDate) != -1) {
                  print("ABORT");
                  print(firstDate);
                  print(lastDate);
                } else {
                  print("PUSH");
                  print(firstDate);
                  print(lastDate);
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => SchedulePage()));
                }
              }),
          FloatingActionButton.extended(
              icon: Icon(Icons.camera_alt), label: Text("Scan"), heroTag: UniqueKey(), backgroundColor: Theme.of(context).primaryColor, onPressed: scanQR),
        ],
      ),
    );
  }

  Widget _buildList(List<Device> devices) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return DevicePreview(devices[index]);
      },
      itemCount: devices.length,
      padding: EdgeInsets.only(bottom: 100.0),
    );
  }

  Widget _buildLoading(String status) {
    return Center(
      child: Column(
        children: <Widget>[
          AdaptiveProgressIndicator(),
          Padding(
            padding: EdgeInsets.only(top: 10),
          ),
          Text(
            status,
            style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 20.0, ),
            textAlign: TextAlign.center,
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return WillPopScope(
          onWillPop: () {
            LogoutDialog.onLogoutPressed(context);
          },
          child: DefaultTabController(
            length: 2,
            child: model.isLoading
                ? Scaffold(
                    appBar: _buildAppBar(context),
                    body: TabBarView(
                      children: [
                        _buildLoading(model.loadingStatus),
                        _buildLoading(model.loadingStatus),
                      ],
                    ),
                  )
                : Scaffold(
                    appBar: _buildAppBar(context),
                    body: TabBarView(
                      children: [
                        _buildList(widget.model.unfinishedDevices),
                        _buildList(widget.model.finishedDevices),
                      ],
                    ),
                    floatingActionButton: _buildFloatingButton(context, model),
                    floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
                  ),
          ),
        );
      },
    );
  }
}
