import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  final String aboutText = 'Dummy about text for daidalos';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Über Daidalos'),
      ),
      body: Center(
        child: Text(aboutText),
      ),
    );
  }
}
