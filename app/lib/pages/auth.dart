import 'dart:async';

import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../scoped-models/main.dart';
import '../widgets/adaptive_progress_indicator.dart';

class AuthPage extends StatefulWidget {
  AuthPageState createState() => AuthPageState();
}

class AuthPageState extends State<AuthPage> {
  TextEditingController passwordController;
  Timer wrongTokenTimer;
  String loginFieldHintText;
  Color loginFlieldTextColor;

  @override
  void initState() {
    super.initState();
    passwordController = TextEditingController();
    wrongTokenTimer = Timer(Duration(), () {});
    loginFieldHintText = "Zugangs-Schlüssel";
  }

  Widget _buildLogo() {
    return Hero(
      tag: UniqueKey(),
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 120.0,
        child: Image.asset('assets/app-icon.png'),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            'DAIDALOS – Assistent für',
            style:
                TextStyle(color: Theme.of(context).accentColor, fontSize: 20),
            textAlign: TextAlign.center,
          ),
          Text(
            'Wartung & Instandhaltung',
            style:
                TextStyle(color: Theme.of(context).primaryColor, fontSize: 30),
            textAlign: TextAlign.center,
          ),
          Text(
            'an der Humboldt-Universität zu Berlin',
            style:
                TextStyle(color: Theme.of(context).accentColor, fontSize: 15),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildPasswordField() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40.0),
      child: TextField(
        controller: passwordController,
        obscureText: true,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          hintText: loginFieldHintText,
          hintStyle: TextStyle(
            color: loginFlieldTextColor,
            fontSize: 20,
          ),
          border: OutlineInputBorder(
              borderSide:
                  BorderSide(width: 2.0, color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(16.0)),
          enabledBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(width: 2.0, color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(16.0)),
        ),
      ),
    );
  }

  Widget _buildLoginButton() {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return model.isLoading
            ? Center(
                child: AdaptiveProgressIndicator(),
              )
            : MaterialButton(
                minWidth: 150.0,
                height: 42.0,
                color: Theme.of(context).primaryColor,
                onPressed: () => _submit(model.authenticate),
                child: Text(
                  "Anmelden",
                  style: TextStyle(
                      color: Colors.white,
                      fontStyle: FontStyle.italic,
                      fontSize: 20.0),
                ),
              );
      },
    );
  }

  void _submit(Function authenticate) async {
    Map<String, dynamic> successInformation = await authenticate(passwordController.text);
    if (successInformation['success']) {
      //Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
    } else {
      setState(() {
        loginFieldHintText = successInformation['message'];
        loginFlieldTextColor = Colors.red;
      });

      wrongTokenTimer = Timer(const Duration(seconds: 2), () {
        setState(() {
          loginFieldHintText = "Zugangs-Schlüssel";
          loginFlieldTextColor = Theme.of(context).accentColor;
        });
      });
      passwordController.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: ListView(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 48.0,
                  ),
                  _buildLogo(),
                  SizedBox(
                    height: 24.0,
                  ),
                  _buildTitle(),
                  SizedBox(
                    height: 48.0,
                  ),
                  _buildPasswordField(),
                  SizedBox(
                    height: 48.0,
                  ),
                  _buildLoginButton(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    passwordController.dispose();
    wrongTokenTimer.cancel();
    super.dispose();
  }
}
