import 'dart:async';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:scoped_model/scoped_model.dart';

import '../scoped-models/main.dart';
import '../models/device.dart';
import '../widgets/helpers/date-display.dart';
import '../widgets/adaptive_progress_indicator.dart';

class SchedulePage extends StatefulWidget {
  _SchedulePage createState() => _SchedulePage();
}

class _SchedulePage extends State<SchedulePage> {
  List<Device> selectableDevices = [];
  final dateFormat = DateFormat('EEEE d.M.yyyy H:mm', 'de');

  DateTime date = DateTime.now();

  Widget _buildListItem(Device device) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return CheckboxListTile(
          title: Text(
            device.title,
            style: TextStyle(
                color: Theme.of(context).primaryColor, fontSize: 30.0),
          ),
          onChanged: (bool value) {
            setState(() {
              model.toggleScheduling(device, value);
            });
          },
          activeColor: Theme.of(context).primaryColor,
          value: device.isSelectedForSchedling,
          subtitle: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(device.id),
                Text(
                    '${DateDisplay.get(device.startDate)} - ${DateDisplay.get(device.endDate)}'),
                Text(device.address),
              ],
            ),
            margin: EdgeInsets.all(10.0),
          ),
        );
      },
    );
  }

  _buildListView() {
    return Expanded(
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return _buildListItem(selectableDevices[index]);
        },
        itemCount: selectableDevices.length,
        padding: EdgeInsets.only(bottom: 100.0),
      ),
    );
  }

  Widget _buildDateTimePickerFormField(MainModel model) {
    DateTime firstDate;
    DateTime lastDate;
    for (final device in model.unfinishedDevices) {
      if (firstDate == null || lastDate == null) {
        firstDate = device.startDate;
        lastDate = device.endDate;
      }
      firstDate = (device.startDate.compareTo(firstDate) == -1)
          ? device.startDate
          : firstDate;
      lastDate =
          (device.endDate.compareTo(lastDate) == 1) ? device.endDate : lastDate;
    }

    return DateTimePickerFormField(
      format: dateFormat,
      style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 20.0),
      decoration: InputDecoration(
        labelText: 'Datum',
      ),
      editable: false,
      dateOnly: false,
      firstDate: firstDate,
      lastDate: lastDate,
      initialDate: DateTime.now(),
      initialTime: TimeOfDay.now(),
      onChanged: (dt) {
        setState(() {
          date = dt;         
          if (dt != null) {
            selectableDevices = [];
            model.unfinishedDevices.forEach((Device device) {
              bool startOK = device.startDate.compareTo(date) == -1;
              bool endOK = device.endDate.compareTo(date) == 1;
              if (startOK && endOK) {
                selectableDevices.add(device);
              } else {
                model.toggleScheduling(device, false);
              }
            });
          } else {
            selectableDevices = [];
          }
        });
      },
    );
  }

  _buildBottomSheet(MainModel model) {
    return model.isLoading
        ? Center(
            child: AdaptiveProgressIndicator(),
          )
        : Container(
            color: Theme.of(context).primaryColor,
            padding: EdgeInsets.all(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    'Absenden',
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                  onPressed: () {
                    model.schedule();
                  },
                ),
              ],
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return WillPopScope(
        onWillPop: () {
          for (final device in model.unfinishedDevices) {
            if (device.isSelectedForSchedling) {
              model.toggleScheduling(device, false);
            }
          }
          if(!model.isLoading) {
            return Future<bool>.value(true);    
          } else {
            return Future<bool>.value(false);
          }
        },
        child: Scaffold(
          appBar: AppBar(title: Text('Datum auswählen')),
          body: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                _buildDateTimePickerFormField(model),
                _buildListView(),
              ],
            ),
          ),
          bottomSheet: _buildBottomSheet(model),
        ),
      );
    });
  }
}
