import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../scoped-models/main.dart';
import '../models/step.dart';

class DevicePage extends StatefulWidget {
  _DevicePageState createState() => _DevicePageState();
}

class _DevicePageState extends State<DevicePage> {
  int codeScanStep = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
          appBar: AppBar(
            title: Text(model.selectedDevice.title),
          ),
          body: Container(
            //child: Center(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Stepper(
                      type: StepperType.vertical,
                      currentStep: model.selectedDevice.current_step,
                      onStepTapped: (step) {
                        setState(() {
                          if (model.selectedDevice.maxStep >= step) {
                            model.selectedDevice.current_step = step;
                          }
                          if (<Type>[Type.FEEDBACK, Type.INFO_PDF, Type.INFO_TEXT].contains(model.selectedDevice.stepData[step].type)) {
                            if (step == model.selectedDevice.maxStep && model.selectedDevice.stepData[step].type != Type.INFO_PDF) {
                              model.setStepDoneStatus(model.selectedDevice.stepData[step].id, true);
                            }
                            model.selectedDevice.current_step = step;
                          }
                        });
                      },
                      steps: model.selectedDevice.stepData.map((StepData stepData) => stepData.getStep(model.selectedDevice.maxStep)).toList(),
                      controlsBuilder: (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                        return Container();
                      },
                    ),
                  ),
                ],
              ),
            //),
          ),
        );
      },
    );
  }
}
