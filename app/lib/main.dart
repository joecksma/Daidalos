import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:scoped_model/scoped_model.dart';

import './pages/auth.dart';
import './pages/device_overview.dart';
import './pages/device.dart';
import './pages/schedule.dart';
import './scoped-models/main.dart';
import './shared/adaptive_theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();
  bool _isAuthenticated = false;

  @override
  void initState() {
    _model.autoAuthenticate();
    _model.userSubject.listen((bool isAuthenticated) {
      setState(() {
        _isAuthenticated = isAuthenticated;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
      model: _model,
      child: MaterialApp(
        title: 'Daidalos',
        theme: getAdaptiveThemeData(context),
        locale: Locale('de', ''),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('de', ''),
        ],
        routes: {
          '/': (BuildContext context) =>
              !_isAuthenticated ? AuthPage() : DeviceOverviewPage(_model),
          '/schedule': (BuildContext context) => SchedulePage(),
        },
        onGenerateRoute: (RouteSettings settings) {
          if (!_isAuthenticated) {
            return MaterialPageRoute<bool>(
              builder: (BuildContext context) => AuthPage(),
            );
          }
          final List<String> pathElements = settings.name.split('/');
          if (pathElements[0] != '') {
            return null;
          }
          if (pathElements[1] == 'device') {
            /*final String deviceId = pathElements[2];
            final Device device = _model.allDevices.firstWhere((Device device) {
              return device.id == deviceId;
            });*/
            return MaterialPageRoute<bool>(
              builder: (BuildContext context) =>
                  !_isAuthenticated ? AuthPage() : DevicePage(),
            );
          }
          return null;
        },
        onUnknownRoute: (RouteSettings settings) {
          return MaterialPageRoute(
              builder: (BuildContext context) =>
                  !_isAuthenticated ? AuthPage() : DeviceOverviewPage(_model));
        },
      ),
    );
  }
}
