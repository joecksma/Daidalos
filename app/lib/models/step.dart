import 'package:flutter/material.dart';

enum Type { SCAN_CONFIRMATION, INFO_PDF, PHOTOS_INPUT, CHECK, INFO_TEXT, FEEDBACK, FINISH }

class StepData {
  final int id;
  final Type type;
  final String title;
  final bool require;
  bool done;
  Widget content;

  StepData({
    @required this.id,
    @required this.type,
    @required this.title,
    @required this.require,
    this.done,
    @required this.content,
  });

  Step getStep(int available) {
    StepState state;

    bool isAvailable = available >= id;
    if (isAvailable && require && !done) {
      state = StepState.error;
    } else if (isAvailable && done) {
      state = StepState.complete;
    } else if (isAvailable && !done) {
      state = StepState.error;
    } else {
      state = StepState.indexed;
    }

    List<Type> allwaysAvailable = [Type.FEEDBACK, Type.INFO_PDF, Type.INFO_TEXT];

    return Step(
      title: Text(title),
      subtitle: Text(''),
      content: content,
      state: state,
      isActive: isAvailable || allwaysAvailable.contains(type),
    );
  }
}
