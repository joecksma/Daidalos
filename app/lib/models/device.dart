import 'package:flutter/material.dart';
import './step.dart';

class Device {
  final String title;
  final String id;
  final String address;
  final DateTime startDate;
  final DateTime endDate;

  bool done;
  bool isSelectedForSchedling;
  bool isCodeChecked;
  List<StepData> stepData;
  int maxStep;
  int current_step;
  
  Device({
    @required this.title,
    @required this.id,
    @required this.address,
    @required this.startDate,
    @required this.endDate,
    @required this.stepData,
    this.done = false,
    this.maxStep = 0,
    this.current_step = 0,
    this.isSelectedForSchedling = false,
    this.isCodeChecked = false
  });
}