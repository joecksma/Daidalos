import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main.dart';
import '../adaptive_progress_indicator.dart';

class Finished extends StatelessWidget {
  final int id;
  Finished(this.id);
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
      return model.selectedDevice.done ? Container() : RaisedButton.icon(
        icon: Icon(Icons.file_upload, color: Colors.white,),
        label: Text('Fertig',
          style: TextStyle(color: Colors.white),
        ),
        elevation: 5.0,
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Wirklich fertig?'),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("NEIN"),
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                    FlatButton(
                      child: Text("JA"),
                      onPressed: () async {
                        model.setStepDoneStatus(id, true);
                        model.sendToServer(context);
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return SimpleDialog(
                                  title: Text('Sende...'),
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                    AdaptiveProgressIndicator()
                                  ],)
                                ],
                              );
                            });
                        Navigator.popUntil(context, ModalRoute.withName('/'));
                      },
                    ),
                  ],
                );
              });
        },
        color: Theme.of(context).primaryColor,
      );
    });
  }
}
