import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';
import 'package:daidalos/scoped-models/main.dart';

class InfoText extends StatelessWidget {
  final text;
  final int id;

  InfoText(this.id, {@required this.text});

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Text(text);
      }
    );
  }
}