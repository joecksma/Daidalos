import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main.dart';

import './photo.dart';

class FeedbackDaidalos extends StatefulWidget {
  _FeedbackDaidalosState createState() => _FeedbackDaidalosState();
  final int id;
  final String text;
  final List<Map<String,dynamic>> photos;
  final TextEditingController controller = TextEditingController();
  FeedbackDaidalos(this.id, {this.text = "", this.photos = const []});
}

class _FeedbackDaidalosState extends State<FeedbackDaidalos> {

  void initState() {
    super.initState();
    widget.controller.text = widget.text;
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(builder: (BuildContext context, Widget child, MainModel model) {
      return Column(
        children: <Widget>[
          TextField(
            controller: widget.controller,
            textAlign: TextAlign.center,
            keyboardType: TextInputType.multiline,
            maxLines: null,
            decoration: InputDecoration(hintText: 'Details'),
          ),
          Photo(
            widget.id,
            photos: widget.photos,
          ),
        ],
      );
    });
  }
}
