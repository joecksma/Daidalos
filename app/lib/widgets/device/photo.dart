import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_view/photo_view.dart';

import '../../scoped-models/main.dart';

class Photo extends StatefulWidget {
  final List<Map<String, dynamic>> photos;
  final int id;
  Photo(this.id, {this.photos});
  photostate createState() => photostate();
}

class photostate extends State<Photo> {
  String _buttonText = 'Foto aufnehmen';
  Future<Null> _pickImageFromCamera(MainModel model) async {
    final File pickedFile = await ImagePicker.pickImage(source: ImageSource.camera);
    if (pickedFile != null) {
      setState(() {
        model.addPhoto(widget.id, pickedFile, context);
        _buttonText = 'Weiteres Foto aufnehmen';
      });
      model.setStepDoneStatus(widget.id, true);
    }
  }

  Widget _buildPickImageButton(MainModel model) {
    return RaisedButton.icon(
      icon: Icon(
        Icons.camera_alt,
        color: Colors.white,
      ),
      label: Text(
        _buttonText,
        style: TextStyle(color: Colors.white, fontSize: 15.0),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () async {
        await _pickImageFromCamera(model);
      },
    );
  }

  Widget photoToWidget(String path) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => PhotoView(
                      imageProvider: FileImage(File(path)),
                    )));
      },
      child: Container(
        margin: EdgeInsets.all(5.0),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          child: Image.file(
            File(path),
            fit: BoxFit.cover,
            width: 1000.0,
          ),
          
        ),
        
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(builder: (BuildContext context, Widget child, MainModel model) {
      if (widget.photos != null && !widget.photos.isEmpty) {
        model.setStepDoneStatus(widget.id, true);
      }
      return Column(
        children: <Widget>[
          widget.photos.isEmpty
              ? Container() :
              widget.photos.length == 1 ?
              Container(
                height: 300.0,
                child:photoToWidget(widget.photos[0]['path']),
              )
              : CarouselSlider(
                  items: widget.photos.map((photo) {
                    return photoToWidget(photo['path']);
                  }).toList(),
                  height: 300.0,
                  autoPlay: true,
                ),
          _buildPickImageButton(model),
        ],
      );
    });
  }
}
