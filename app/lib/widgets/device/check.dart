import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main.dart';

class Check extends StatefulWidget {
  _CheckState createState() => _CheckState();
  final int id;
  final String description;
  Check(this.id, [this.description = '']);
}

class _CheckState extends State<Check> {
  bool value;
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
      value = model.selectedDevice.stepData[widget.id].done;
      return Column(
        children: <Widget>[
          Text(widget.description),
          Checkbox(
            checkColor: Colors.white,
            activeColor: Theme.of(context).primaryColor,
            onChanged: (bool newState) {
              model.setStepDoneStatus(widget.id, newState);
              setState(() {
                value = newState;
              });
            },
            value: value,
          ),
        ],
      );
    });
  }
}
