import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main.dart';

class Scan extends StatefulWidget {
  final int id;
  Scan(this.id);
  _ScanState createState() => _ScanState();
}

class _ScanState extends State<Scan> {
  String qrScanResult;

  Future<void> showError(String error) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(title: Text("Error"), content: Text(error));
      },
    );
  }

  @override
  void initState() {
    super.initState();
    qrScanResult = "";
  }
 
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {

        return RaisedButton.icon(
          icon: Icon(
            model.selectedDevice.stepData[widget.id].done ? Icons.check : Icons.camera_alt,
            color: Colors.white,
          ),
          label: Text(
            "Scan",
            style: TextStyle(color: Colors.white, fontSize: 15.0),
          ),
          color: Theme.of(context).primaryColor,
          onPressed: model.selectedDevice.stepData[widget.id].done ? null : () async {
            try {
              String qrResult = await BarcodeScanner.scan();
              setState(() {
                qrScanResult = qrResult;
                if (model.selectedDeviceId != qrResult) {
                  showError('Gescannter Code gehört nicht zum erwarteten Objekt!');
                } else {
                  model.selectedDevice.stepData[widget.id].done = true;
                  //model.setCodeScanned(true);
                  model.setStepDoneStatus(widget.id, true);
                  //do nothing
                }
              });
            } on PlatformException catch (ex) {
              if (ex.code == BarcodeScanner.CameraAccessDenied) {
                showError("Kamera-Zugriff wurde verweigert!");
              } else {
                showError("Unknown Error $ex\nScan-Result: $qrScanResult");
              }
            } on FormatException {} on StateError catch (ex) {
              showError(
                  "Dieses Gerät ist nicht Teil Ihres Wartungsauftrags!\n$ex\nScan-Result: $qrScanResult");
            } catch (ex) {
              showError("Unknown Error $ex\nScan-Result: $qrScanResult");
            }
          },
        );
      },
    );
  }
}
