import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';

import '../../scoped-models/main.dart';

class Pdf extends StatelessWidget {
  final String url;
  final String file;
  final String filename;
  final int id;
  Pdf(this.id, this.file, this.filename, this.url);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(builder: (BuildContext context, Widget child, MainModel model) {
      return RaisedButton.icon(
        icon: Icon(Icons.insert_drive_file, color: Colors.white),
        label: Text(
          "$filename anzeigen.",
          style: TextStyle(color: Colors.white),
        ),
        color: Theme.of(context).primaryColor,
        onPressed: () {
          model.setStepDoneStatus(id, true);
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => PDFViewerScaffold(
                      appBar: AppBar(
                        title: Text(filename),
                      ),
                      path: file,
                    ),
              ));
        },
      );
    });
  }
}
