class DateDisplay {
  static String get(DateTime date) {
    return '${date.day}.${date.month}.${date.year}';
  }
}