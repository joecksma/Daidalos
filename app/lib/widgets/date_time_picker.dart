import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class DateTimePicker extends StatefulWidget {
  _DateTimePickerState createState() => _DateTimePickerState();
}

class _DateTimePickerState extends State<DateTimePicker> {
  final dateFormat = DateFormat('EEEE d.M.yyyy H:mm', 'de');
  final timeFormat = DateFormat("H:mm", 'de');
  DateTime date;
  TimeOfDay time;
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: Text('Datum auswählen')),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: ListView(
          children: <Widget>[
            DateTimePickerFormField(
              format: dateFormat,
              decoration: InputDecoration(labelText: 'Datum'),
              onChanged: (dt) => setState(() => date = dt),
            ),
          ],
        ),
      ));
}