import 'dart:async';

import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';
import '../scoped-models/main.dart';

class LogoutDialog {
  static Future<bool> onLogoutPressed(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text("Wirklich abmelden?"),
              actions: <Widget>[
                _buildClosePopupButton(context),
                _buildConfirmLogoutButton(context),
              ]);
        });
  }

  static Widget _buildClosePopupButton(BuildContext context) {
    return FlatButton(
      child: Text("NEIN"),
      onPressed: () {
        Navigator.pop(context, false);
      },
    );
  }

  static Widget _buildConfirmLogoutButton(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return FlatButton(
          child: Text("JA"),
          onPressed: () async {
            await model.logout();
            Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
          },
        );
      },
    );
  }
}
