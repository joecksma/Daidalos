import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../models/device.dart';
import '../scoped-models/main.dart';
import '../pages/device.dart';
import './helpers/date-display.dart';

class DevicePreview extends StatelessWidget {
  final Device device;

  DevicePreview(this.device);

  Widget _buildRightArrow(BuildContext context) {
    return Container(
      decoration: ShapeDecoration(
        shape: CircleBorder(),
        color: Theme.of(context).primaryColor,
      ),
      child: Icon(
        Icons.keyboard_arrow_right,
        color: Colors.white,
        size: 40.0,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  model.selectDevice(device.id);
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => DevicePage()));
                },
                child: Card(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            device.title,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 30.0),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(device.id),
                          Text(
                              '${DateDisplay.get(device.startDate)} - ${DateDisplay.get(device.endDate)}'),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(device.address),
                              _buildRightArrow(context)
                            ],
                          )
                        ]),
                    margin: EdgeInsets.all(10.0),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
