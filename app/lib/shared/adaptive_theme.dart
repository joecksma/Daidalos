import 'package:flutter/material.dart';

const _HUBlueHEX = 0xFF165096;
const _HUGreyHEX = 0xFF6d6d6d;

const MaterialColor _HUBLUE = const MaterialColor(
  _HUBlueHEX,
  const <int, Color>{
      50:  const Color(_HUBlueHEX),
      100: const Color(_HUBlueHEX),
      200: const Color(_HUBlueHEX),
      300: const Color(_HUBlueHEX),
      400: const Color(_HUBlueHEX),
      500: const Color(_HUBlueHEX),
      600: const Color(_HUBlueHEX),
      700: const Color(_HUBlueHEX),
      800: const Color(_HUBlueHEX),
      900: const Color(_HUBlueHEX),
  },
);
const MaterialColor _HUGREY = const MaterialColor(
  _HUGreyHEX,
  const <int, Color>{
      50:  const Color(_HUGreyHEX),
      100: const Color(_HUGreyHEX),
      200: const Color(_HUGreyHEX),
      300: const Color(_HUGreyHEX),
      400: const Color(_HUGreyHEX),
      500: const Color(_HUGreyHEX),
      600: const Color(_HUGreyHEX),
      700: const Color(_HUGreyHEX),
      800: const Color(_HUGreyHEX),
      900: const Color(_HUGreyHEX),
  },
);


final ThemeData _androidTheme = ThemeData(
  brightness: Brightness.light,
  primarySwatch: _HUBLUE,//Color.fromRGBO(22, 80, 150, 255),
  primaryColor: _HUBLUE,
  accentColor: _HUGREY,//Color.fromRGBO(109, 109, 109, 255),
  buttonColor:  _HUBLUE//Color.fromRGBO(22, 80, 150, 255),
);

final ThemeData _iOSTheme = ThemeData(
  brightness: Brightness.light,
  primarySwatch: _HUBLUE,//Color.fromRGBO(22, 80, 150, 255),
  primaryColor: _HUBLUE,
  accentColor: _HUGREY,//Color.fromRGBO(109, 109, 109, 255),
  buttonColor:  _HUBLUE//Color.fromRGBO(22, 80, 150, 255),
);

ThemeData getAdaptiveThemeData(context) {
  return Theme.of(context).platform == TargetPlatform.android ? _androidTheme : _iOSTheme;
}
