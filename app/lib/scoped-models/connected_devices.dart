import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path_provider/path_provider.dart';

import '../models/device.dart';
import '../models/user.dart';
import '../models/step.dart';

import '../widgets/device/pdf.dart';
import '../widgets/device/photo.dart';
import '../widgets/device/scan.dart';
import '../widgets/device/infoText.dart';
import '../widgets/device/check.dart';
import '../widgets/device/feedback.dart';
import '../widgets/device/finished.dart';

class ConnectedDevicesModel extends Model {
  String URL = 'http://141.20.37.114'; //TODO final Url
  List<Device> _devices = [];
  String _selDeviceId;
  User _authenticatedUser;
  bool _is_Loading = false;
  String _loading_status = '';
}

class DeviceModel extends ConnectedDevicesModel {
  List<Device> get allDevices {
    return List.from(_devices);
  }

  List<Device> get finishedDevices {
    return _devices.where((Device device) => device.done).toList();
  }

  List<Device> get unfinishedDevices {
    return _devices.where((Device device) => !device.done).toList();
  }

  int get selectedDeviceIndex {
    return _devices.indexWhere((Device device) {
      return device.id == _selDeviceId;
    });
  }

  String get selectedDeviceId {
    return _selDeviceId;
  }

  Device get selectedDevice {
    if (_selDeviceId == null) {
      return null;
    }
    return _devices.firstWhere((Device device) {
      return device.id == _selDeviceId;
    });
  }

  void selectDevice(String deviceId) {
    print('SELECT $deviceId');
    _selDeviceId = deviceId;
    if (deviceId != null) {
      notifyListeners();
    }
  }

  String printPrettyJson(String jsonS) {
    var encoder = new JsonEncoder.withIndent("  ");
    String body = encoder.convert(json.decode(jsonS));
    debugPrint(body);
    return body;
  }

  Future<File> downloadFile(String url, {String logTitle}) async {
    String filename = url.substring(url.lastIndexOf("/") + 1);
    if (filename.contains('=')) {
      filename = filename.substring(filename.lastIndexOf('=') + 1);
    }
    print('download: ($logTitle) \n\t$url');
    _loading_status = '$logTitle: Lade $filename runter';
    notifyListeners();
    String finalUrl = url;
    if (url.toString().startsWith(URL)) {
      finalUrl = url + '&auth=${_authenticatedUser.token}';
    }
    print(finalUrl);
    var request = await HttpClient().getUrl(Uri.parse(finalUrl));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File newfile = new File('$dir/$filename');
    await newfile.writeAsBytes(bytes);
    print('download finished: ($logTitle)\n\t$url -> ${dir}/${filename}');
    return newfile;
  }

  Future<Null> fetchDevices({clearExisting = false}) async {
    print("Start fetch");
    _is_Loading = true;
    _loading_status = 'Lade Wartungen vom Server';
    notifyListeners();
    if (clearExisting) {
      _devices = [];
    }
    return http.get('${URL}/devices.php?auth=${_authenticatedUser.token}').then<Null>((http.Response response) async {
      final List<Device> fetchedDeviceList = [];
      if (response.body == null || response.body == '') {
        print("FAILED: fetched data was empty");
        _is_Loading = false;
        notifyListeners();
        //return;
      }
      print('BODY:');
      printPrettyJson(response.body);

      final Map<String, dynamic> deviceListData = json.decode(response.body);

      if (deviceListData == null) {
        print("FAILED");
        _is_Loading = false;
        notifyListeners();
        return;
      }

      await deviceListData.forEach((String deviceId, dynamic deviceData) async {
        List<StepData> stepData = [];
        List<dynamic> rawStepData = deviceData['steps'];
        _loading_status = 'Verarbeite Daten';
        notifyListeners();
        for (int id = 0; id < rawStepData.length; id++) {
          Type type = Type.values.firstWhere((e) => e.toString() == 'Type.' + rawStepData[id]['type']);

          Widget content;
          switch (type) {
            case Type.SCAN_CONFIRMATION:
              content = Scan(id);
              break;
            case Type.INFO_PDF:
              if (rawStepData[id]['content'] != null) {
                File newfile = await downloadFile(rawStepData[id]['content'], logTitle: deviceData['title']);
                String filename = newfile.path.substring(newfile.path.lastIndexOf('/') + 1);
                content = Pdf(id, newfile.path, filename, rawStepData[id]['content']);
              }
              break;
            case Type.INFO_TEXT:
              String text = rawStepData[id]['content'] != null ? rawStepData[id]['content'] : '';
              content = InfoText(id, text: text);
              break;
            case Type.PHOTOS_INPUT:
              List<Map<String, dynamic>> photos = [];
              if (rawStepData[id]['data'] != null) {
                for (int i = 0; i < rawStepData[id]['data'].length; i++) {
                  String url = rawStepData[id]['data'][i];
                  if (url != null) {
                    File newfile = await downloadFile(url, logTitle: deviceData['title']);
                    print("\t Path: ${newfile.path}");
                    photos.add({'path': newfile.path, 'online': true, 'url': url});
                  }
                }
              }
              content = Photo(
                id,
                photos: photos,
              );
              break;
            case Type.CHECK:
              String description = rawStepData[id]['content'];
              content = Check(
                id,
                description,
              );
              break;
            case Type.FEEDBACK:
              String text = rawStepData[id]['data']['text'] != null ? rawStepData[id]['text'] : '';
              List<Map<String, dynamic>> photos = [];
              if (rawStepData[id]['data']['photos'] != null) {
                for (int i = 0; i < rawStepData[id]['data']['photos'].length; i++) {
                  String url = rawStepData[id]['data']['photos'][i];
                  if (url != null) {
                    File newfile = await downloadFile(url, logTitle: deviceData['title']);
                    photos.add({'path': newfile.path, 'online': true, 'url': url});
                  }
                }
              }
              content = FeedbackDaidalos(
                id,
                text: text,
                photos: photos,
              );
              break;
            case Type.FINISH:
              content = Finished(id);
          }

          stepData.add(StepData(
              id: id,
              type: type,
              title: (rawStepData[id]['title'] != null) ? rawStepData[id]['title'] : 'Schritt',
              content: content,
              require: (rawStepData[id]['required'] != null) ? rawStepData[id]['required'] : true,
              done: (rawStepData[id]['done'] != null) ? rawStepData[id]['done'] : false));
        }
        stepData.add(StepData(
            id: stepData.length,
            type: Type.FINISH,
            title: 'Absenden',
            content: Finished(stepData.length),
            require: true,
            done: deviceData['done'] == 'true'));
        print("\tfetched: " + deviceId);
        final Device device = Device(
            id: deviceId,
            title: deviceData['title'],
            address: deviceData['address'],
            startDate: DateTime.parse(deviceData['startDate']),
            endDate: DateTime.parse(deviceData['endDate']),
            stepData: stepData,
            done: deviceData['done'].toString().toLowerCase() == 'true');
        fetchedDeviceList.add(device);
        if (fetchedDeviceList.length == deviceListData.length) {
          print("FINISHED FETCH");
          _devices = fetchedDeviceList;
          _is_Loading = false;
          notifyListeners();
          _selDeviceId = null;
          _loading_status = '';
        }
      });
    }).catchError((error) {
      print('FAILED FETCH');
      print(error.toString());
      _is_Loading = false;
      notifyListeners();
      return;
    });
  }

  Future<String> _uploadImage(String filePath, [int current = 1, int total = 1, String log = '']) async {
    File file = File(filePath);
    if (file == null) return null;
    _loading_status = 'Lade Bild(er) für Schritt:\n$log\nvon\n${selectedDevice.title}\nhoch\n[$current/$total]';

    String base64Image = base64Encode(file.readAsBytesSync());
    String fileName = file.path.split("/").last;
    print('UPLOAD_IMAGE $log[$current/$total] $fileName ($selectedDeviceId) ');

    String url = '';

    await http.post('${URL}/upload.php?device=${selectedDeviceId}&auth=${_authenticatedUser.token}', body: {
      "image": base64Image,
      "name": fileName,
    }).then((res) {
      Map<String, dynamic> data = json.decode(res.body);
      print('Recieved Data:');
      printPrettyJson(res.body);
      if (data != null) {
        if (data['success'] == true) {
          print('FINISHED UPLOADING $filePath ($selectedDeviceId) \n\t -> ${data["url"]}');
          url = data['url'];
          return data['url'];
        } else {
          print('ERROR : success = false');
          return null;
        }
      } else {
        print('ERROR : recieved no data');
        return null;
      }
    }).catchError((error) {
      print('FAILED UPLOADING: $filePath ($selectedDeviceId)');
      print(error);
      return null;
    });
    return url;
  }

  Future<dynamic> sendToServer(BuildContext context) async {
    selectedDevice.done = true;
    _is_Loading = true;
    _loading_status = 'Sende ${selectedDevice.title} an Server';
    print('SENDING $selectedDeviceId');
    notifyListeners();

    List<dynamic> steps = [];

    for (int d = 0; d < selectedDevice.stepData.length; d++) {
      StepData deviceData = selectedDevice.stepData[d];

      if (deviceData.type != Type.FINISH) {
        Map<String, dynamic> step = {
          'type': deviceData.type.toString().split('.')[1],
          'title': deviceData.title,
          'required': deviceData.require,
          'done': deviceData.done,
        };

        switch (deviceData.type) {
          case Type.INFO_PDF:
            Pdf content = deviceData.content;
            step['content'] = content.url;
            break;
          case Type.INFO_TEXT:
            InfoText content = deviceData.content;
            step['content'] = content.text;
            break;
          case Type.PHOTOS_INPUT:
            Photo content = deviceData.content;
            List<dynamic> data = [];
            for (int i = 0; i < content.photos.length; i++) {
              if (!content.photos[i]['online']) {
                content.photos[i]['url'] = await _uploadImage(content.photos[i]['path'], i, content.photos.length, deviceData.title);
              }
              data.add(content.photos[i]['url']);
              print("ADD: ${content.photos[i]['url']}");
            }
            if (!data.isEmpty) {
              step['data'] = data;
            }
            break;
          case Type.FEEDBACK:
            FeedbackDaidalos content = deviceData.content;
            Map<String, dynamic> data = {};
            data['text'] = content.controller.text;
            List<dynamic> photos = [];
            for (int i = 0; i < content.photos.length; i++) {
              if (!content.photos[i]['online']) {
                content.photos[i]['url'] = await _uploadImage(content.photos[i]['path'], i, content.photos.length, deviceData.title);
              }
              photos.add(content.photos[i]['url']);
            }
            if (!photos.isEmpty) {
              data['photos'] = photos;
            }
            step['data'] = data;
            break;
          case Type.CHECK:
            Check content = deviceData.content;
            step['content'] = content.description;
            break;
          default:
            break;
        }
        steps.add(step);
      }
    }

    Map<String, dynamic> body = {
      'title': selectedDevice.title,
      'startDate': selectedDevice.startDate.toString(),
      'endDate': selectedDevice.endDate.toString(),
      'address': selectedDevice.address,
      'done': selectedDevice.done,
      'steps': steps,
    };
    print('UPLOAD: $selectedDeviceId');
    String jsonPretty = printPrettyJson(json.encode(body));
    return await http
        .post('${URL}/finish.php?device=${selectedDeviceId}&auth=${_authenticatedUser.token}', body: {"json": json.encode(body)}).then((response) {
      print('FINISHED UPLOAD $selectedDeviceId');
      printPrettyJson(response.body);
      selectedDevice.done = true;
      _is_Loading = false;
      _loading_status = '';
      notifyListeners();
    }).catchError((error) {
      _is_Loading = false;
      _loading_status = '';
      notifyListeners();
      print('FAILED UPLOAD $selectedDeviceId');
      print(error);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(title: Text("Fehler!"), content: Text("Keine Verbindung zum Server! Versuchen Sie es später erneut."), actions: <Widget>[
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ]);
        },
      );
    });
  }

  void setCodeScanned(bool value) {
    for (int i = 0; i < selectedDevice.stepData.length; i++) {
      if (selectedDevice.stepData[i].type == Type.SCAN_CONFIRMATION) {
        print("Scan " + i.toString() + " " + value.toString());
        setStepDoneStatus(i, value);
      }
    }
  }

  void addPhoto(int stepId, File photo, BuildContext context) {
    if (selectedDevice.stepData[stepId].content is Photo) {
      Photo content = selectedDevice.stepData[stepId].content;
      content.photos.add({'path': photo.path, 'online': false});
    } else if (selectedDevice.stepData[stepId].content is FeedbackDaidalos) {
      FeedbackDaidalos content = selectedDevice.stepData[stepId].content;
      content.photos.add({'path': photo.path, 'online': false});
    }
    notifyListeners();
  }

  void setStepDoneStatus(int stepId, bool done) {
    selectedDevice.stepData[stepId].done = done;
    int i;
    for (i = 0; i < selectedDevice.stepData.length; i++) {
      if (!selectedDevice.stepData[i].done && selectedDevice.stepData[i].require) {
        break;
      }
    }
    selectedDevice.maxStep = i;
    notifyListeners();
  }

  void toggleScheduling(Device device, bool value) {
    print('TOGGLE_SCHEDULE ${device.id} : $value');
    device.isSelectedForSchedling = value;
    notifyListeners();
  }

  Future<bool> schedule() async {
    _is_Loading = true;
    notifyListeners();
    List<Device> result = unfinishedDevices.where((Device device) {
      return device.isSelectedForSchedling;
    }).toList();
    if (!result.isEmpty) {
      print('SCHEDULE ${result.map((device) => "${device.id}").toList()}');
      //TODO send to backend
      for (final device in result) {
        toggleScheduling(device, false);
      }
    }
    return await Future<bool>.delayed(Duration(seconds: 2), () {
      _is_Loading = false;
      notifyListeners();
    });
  }
}

class UserModel extends ConnectedDevicesModel {
  PublishSubject<bool> _userSubject = PublishSubject();

  User get user {
    return _authenticatedUser;
  }

  PublishSubject<bool> get userSubject {
    return _userSubject;
  }

  Future<Map<String, dynamic>> authenticate(String token) async {
    print("AUTH");
    _is_Loading = true;
    notifyListeners();

    bool hasError = true;
    String message = 'ERROR';
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      await http.get('${URL}/auth.php?auth=${token}').timeout(Duration(seconds: 5)).then<Null>((http.Response response) {
        if (response.body == null || response.body == "") {
          print('no connection');
          message = 'Keine Verbindung zum Server.';
          _is_Loading = false;
          return Future<Map<String, dynamic>>.value({'success': !hasError, 'message': message});
        } else {
          final Map<String, dynamic> data = json.decode(response.body);
          print(" | Response: " + data.toString());
          if (data == null) {
            message = 'Etwas ist etwas schief gegangen.';
            _is_Loading = false;
            return Future<Map<String, dynamic>>.value({'success': !hasError, 'message': message});
          } else if (data['success'] == true) {
            hasError = false;
            message = 'Zugang erfolgreich!';
            _authenticatedUser = User(token: token);
            _userSubject.add(true);
            prefs.setString('token', token);
          } else {
            message = 'Ungültiger Zugangsschlüssel!';
            _is_Loading = false;
            return Future<Map<String, dynamic>>.value({'success': !hasError, 'message': message});
          }
        }
      });
    } catch (e) {
      message = 'Keine Internetverbindung';
      _is_Loading = false;
      return await Future<Map<String, dynamic>>.value({'success': !hasError, 'message': message});
    }
    print(" | Result: " + message);
    _is_Loading = false;
    notifyListeners();

    return await Future<Map<String, dynamic>>.value({'success': !hasError, 'message': message});
  }

  void autoAuthenticate() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    if (token != null) {
      _authenticatedUser = User(token: token);
      _userSubject.add(true);
      notifyListeners();
    }
  }

  void logout() async {
    print('LOGOUT');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    print('DELETING FILES');
    var dir = await getApplicationDocumentsDirectory();
    await dir.delete(recursive: true);
    var tmpDir = await getTemporaryDirectory();
    await tmpDir.delete(recursive: true);
    _authenticatedUser = null;
    _selDeviceId = null;
    _userSubject.add(false);
    print('FINISHED LOGOUT');
  }
}

class UnilityModel extends ConnectedDevicesModel {
  bool get isLoading {
    return _is_Loading;
  }

  String get loadingStatus {
    return _loading_status;
  }
}
