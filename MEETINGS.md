## 2018-12-17 - Prüfen des Backlogs

1. Daily Sprints
2. Überlegen Abgabedatum (Prüfungstermin?, etc...)
3. Überlegen Lizenz
4. Überlegen Publizeirung der App/Demos
5. (Selbst)überlegen der Architektur FTLs/Static+REST, SQL/noSQL/JSON

## 2018-12-10
Treffen mit der TA: Vorstellung der bisherigen Entwürfe; diese werden nun 
intern weiter besprochen. Wichtig ist die Kenntlichmachung von Aprechpartnern in 
Backend und App durch Telefon und Mailadresse. Dazu soll den Wartungsfirmen die 
Möglichkeit gegeben werden, in der App eine/mehrere Wartungen gleichzeitig anzukündigen 
und selbstständig einen Termin festzulegen. Der zuständige HU-Mitarbeiter vor Ort 
(der, der bspw. einen Schlüssel für den Raum besitzt) soll hierüber informiert 
werden, entweder per Mail oder durch einen eigenen Backend-Zugang.

## 2018-11-26
1. Setup von Flutter
2. Logodesign
3. Interface-Entwurft konkretisiert
    ![Interface](docs/drafts/Interface.jpeg)
4. Erste implementation der App-Start-Page
    ![App-Start-Page](docs/drafts/app-startpage.png)

## 2018-11-19
1. Besprechung des Treffens mit der TA am 7.11.18
2. Sammlung der Ideen für User Stories in Trello
3. Zeichnen des [Interface-Entwurfs](docs/drafts/interface.pdf)

## 2018-11-12
1. Besprechung des Treffens mit der TA am 7.11.18
2. Besprechung der Architektur einer Beispielanwendung
    - Was sollte auf der Serverseite sein?
    - Online/offline Funktionalität der Mobile-App
    - Beispielaufteilung der Anwendungsansichten 
    ![Beispielaufteilung](docs/drafts/general.jpeg)
