>## [Semesterprojekt WS 2018/19 - Objektmanagement an der HU](https://www.informatik.hu-berlin.de/de/forschung/gebiete/pda/teaching/ws1819/omhu-1819)

>## [Demo](https://daidalos-backend.firebaseapp.com/)
![App-Start-Page](docs/drafts/app-startpage.png)
![Interface](docs/drafts/Interface.jpeg)

# Dezentrale Datenerfassung
Markierung und Wartung von Anlagen unterstützt durch IT Verfahren

## Group
1. Piotr Witkowski, witkowsp@hu-berlin.de
2. Niklas Deckers, deckersn@hu-berlin.de
3. Maximilian Joecks, joecksma@hu-berlin.de
4. Mateusz Kowalski, kowalsmq@hu-berlin.de

## Links
- [API Examples](https://daidalos-backend.firebaseapp.com/api)
- [Web Dashboard demo](http://daidalos-backend.firebaseapp.com/)
- [TA Musterdaten](https://box.hu-berlin.de/d/612cee1987574f7d98ae/)
- [Trello](https://trello.com/b/lZg2Xc8N/semesterprojekt2018-19team3)

## Exemplary use case
![Folie1](docs/overview1.png)
![Folie2](docs/overview2.png)

## Lizenz
Aufgrund der Nutzung von den packages: 'flutter_full_pdf_viewer' und 'photo_view' muss die App unter einer zur MIT-Lizenz kompatiblen Lizenz stehen!



