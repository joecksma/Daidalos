# Use Cases

## WARTUNGSMITARBEITER
1. Token authentifizierung

2. Alle Wartungsunterlagen herunterladen  

3. Termin vereinbaren:  
3.1. Datum und Uhrzeit festlegen  
3.2. mehrere Anlagen auswählen  
3.3. E-Mails verschicken

4. Wartung durchführen  
4.1. Eine Wartung auf dem Handy starten  
4.2. Checks/Fotos/Kommentare hinzufügen  
4.3. Wartungs als fertig markieren

5. Logout

## WARTUNGSFIRMA erbt von WARTUNGSMITARBEITER
1. Login beim Dashboard
2. Neue Tokens/Wartungsmitarbeiter erstellen
3. Namen+Mitarbeiterinformationen im Popup vergeben
4. Anlagen delegieren / drag and drop verschieben
5. Tokenexport per Mail/Text/QR-Code

## SACHBEARBEITER
1. "Anmeldung"
2. Alle Wartungsvorgänge anzeigen  
2.1. Auftrag erstellen  
2.2. Auftrag bearbeiten  
2.3. Auftrag löschen  
3. Firmenverwaltung  
3.1. Firma hinzufügen  
3.2. Firma bearbeiten  
3.3. Firma löschen
4. Logout

5. OPTIONAL: Anlagenverwaltung
6. OPTIONAL: Templatesverwaltung

## VERANTWORTLICHER VOR ORT
1. E-Mail bekommen
 